module zerolog-demo

go 1.14

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/go-chi/chi v1.5.4
	github.com/go-sql-driver/mysql v1.5.0
	github.com/jinzhu/gorm v1.9.16
	github.com/rs/zerolog v1.20.0
)
