//Zerolog log level priority higher->lower
panic (zerolog.PanicLevel, 5)
fatal (zerolog.FatalLevel, 4)
error (zerolog.ErrorLevel, 3)
warn (zerolog.WarnLevel, 2)
info (zerolog.InfoLevel, 1)
debug (zerolog.DebugLevel, 0)
trace (zerolog.TraceLevel, -1)

1.UNIX Time is faster and smaller than most timestamps
   zerolog.TimeFieldFormat = zerolog.TimeFormatUnix --->ln-1
   log.Info().Msg("info level message")
   //output: {"level":"info","time":1615687262,"message":"info level message"}
   //output(hide ln-1):{"level":"info","time":"2021-03-14T08:04:14+06:00","message":"info level message"}

2.Contextual logging
    log.Debug().Str("Scale", "833 cents").Float64("Interval", 833.09).Msg("Fibonacci is everywhere")
  //You'll note in the above example that when adding contextual fields, the fields are strongly typed.

3.Leveled Logging
     //retrieving flag from command(ex: go run main.go -debug).    
     debug := flag.Bool("debug", false, "sets log level to debug")
	 flag.Parse()
	// Default level for this example is info, unless debug flag is present
	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	if *debug {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}
	log.Debug().Msg("This message appears only when log level set to Debug")
	log.Info().Msg("This message appears when log level set to Debug or Info")
	if e := log.Debug(); e.Enabled() {
		// Compute log output only if enabled.
		value := "bar"
		e.Str("foo", value).Msg("some debug message")
	}
   //output(no flag): 
            {"level":"info","time":1615687866,"message":"This message appears when log level set to Debug or Info"}
   //output(flag -debug):
            {"level":"debug","time":1615687882,"message":"This message appears only when log level set to Debug"}
            {"level":"info","time":1615687882,"message":"This message appears when log level set to Debug or Info"}
            {"level":"debug","foo":"bar","time":1615687882,"message":"some debug message"}

4.Logging without Level or Message
    log.Log(). Str("foo", "bar"). Msg("")

5.Error Logging:You can log errors using the Err method

6.

7.