package main

import (
	"encoding/json"
	"fmt"
	"github.com/BurntSushi/toml"
	"github.com/go-chi/chi"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

//func (d connection) Configure(){
//	dbURI := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?parseTime=true", d.DBUserName, d.DBPassword, d.DBHost, d.DBPort, d.DBSchema);
//	gormConn, err := gorm.Open("mysql", dbURI)
//	if err != nil {
//		log.Print("Could not connect database")
//	}
//	d.DBCon=gormConn;
//}
//func (d connection) GetConnection() *gorm.DB{
//	return d.DBCon;
//}
type DBConnection struct {
	DBHost     string
	DBUserName string
	DBPassword string
	DBSchema   string
	DBPort     int
}


func main(){
	HelloServiceDatabase{}.k();
}

type HelloServiceDatabase struct {
	HelloServiceDatabase DBConnection
}

func (s HelloServiceDatabase) k(){
	//var favorites songs
	if _, err := toml.DecodeFile("config.toml", &s); err != nil {
		fmt.Println("Error",err)
	}
	fmt.Println(s.HelloServiceDatabase.DBPort)

	//if _, tomlErr := toml.DecodeFile("config.toml", &s.HelloServiceDatabase); tomlErr != nil {
	//	log.Error().Msgf("Error reading the configuration file %s.  Error is: %s", os.Getenv("ConfigPath"), tomlErr.Error())
	//}
	//fmt.Println(s.HelloServiceDatabase.DBSchema);
}


func (s *HelloServiceDatabase) Configure(){
	//Set the component of zerolog for this package
	log.Info().Msg("Component value set for DataProvider logger")

	if _, tomlErr := toml.DecodeFile("config.toml", &s); tomlErr != nil {
		log.Error().Msgf("Error reading the configuration file %s.  Error is: %s", os.Getenv("ConfigPath"), tomlErr.Error())
		panic(s)
	}
	//Configuring DB connection pool for listener
	log.Info().Msg("Configuring DB connection pool for listeners")
	//s.HelloServiceDatabase.Configure()
}













var DB *gorm.DB
func mains() {
	output := zerolog.ConsoleWriter{Out: os.Stdout, TimeFormat: time.RFC3339}
	output.FormatLevel = func(i interface{}) string {
		return strings.ToUpper(fmt.Sprintf("| %-6s|", i))
	}
	output.FormatMessage = func(i interface{}) string {
		return fmt.Sprintf("*%s*", i)
	}
	output.FormatFieldName = func(i interface{}) string {
		return fmt.Sprintf("%s", i)
	}
	output.FormatFieldValue = func(i interface{}) string {
		return strings.ToUpper(fmt.Sprintf("%s", i))
	}

	log := zerolog.New(output).With().Timestamp().Logger()

	log.Info().Str("foo", "bar").Msg("Hello World")
	log.Debug().Str("foo", "bar").Msg("This is debuging line")
	log.Info().Str("foo", "bar").Msg("Table column adding")
	log.Info().Str("foo", "bar").Msg("Inserting into table")
	log.Debug().Str("foo", "bar").Msg("We got some values")
	log.Info().Msg("Deleting the old table")
	fmt.Println("Server listen at :8005")
	http.ListenAndServe(":8005",BookRouter())
}

func  BookRouter() http.Handler {
	DB =Instantiate().DB;
	r := chi.NewRouter()
	r.Post("/create-book", CreateBook)
	r.Put("/update-book/{id:[0-9]+}",UpdateBook)
	r.Get("/get-all-book",GetAllBook)
	r.Get("/get-book-by-id/{id:[0-9]+}", GetBookById)
	r.Delete("/delete-book-by-id/{id:[0-9]+}",DeleteBookById)
	r.NotFound(NotFound)
	log.Error();
	return r
}
func NotFound(w http.ResponseWriter, r *http.Request) {

	//http.Redirect(w, r, "http://cma.ca", http.StatusNotFound)
	url := "https://api.ipify.org?format=text"
	// we are using a pulib IP API, we're using ipify here, below are some others
	// https://www.ipify.org
	// http://myexternalip.com
	// http://api.ident.me
	// http://whatismyipaddress.com/api
	resp, err := http.Get(url)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	ip, err := ioutil.ReadAll(resp.Body)
	w.Write([]byte(ip))
}

func CreateBook(writer http.ResponseWriter, request *http.Request) {
	var bk Book;
	err:=json.NewDecoder(request.Body).Decode(&bk)
	if err!=nil{
		panic(err)
	}
	DB.Create(&bk);
	writer.Write([]byte("Successfully created with id_"+strconv.Itoa(int(bk.ID))));
}
func GetBookById(writer http.ResponseWriter, request *http.Request) {
	var bk Book;
	id:=chi.URLParam(request,"id");
	DB.Where("ID=?",id).Find(&bk)
	newsJson, _ := json.Marshal(bk)
	writer.Header().Set("Content-Type", "application/json")
	writer.Write(newsJson)
}
func GetAllBook(writer http.ResponseWriter, request *http.Request) {
	var bk[] Book;
	DB.Find(&bk);
	newsJson, err := json.Marshal(bk)
	if err != nil {
		panic(err)
	}
	writer.Header().Set("Content-Type", "application/json")
	writer.Write(newsJson)
}
func UpdateBook(writer http.ResponseWriter, request *http.Request) {
	var bk Book; var temp Book;
	err:=json.NewDecoder(request.Body).Decode(&temp)
	id:=chi.URLParam(request,"id");
	DB.Find(&bk).Where("ID=?",id)
	bk.BookName=temp.BookName; bk.BookAuthor=temp.BookAuthor; bk.Price=temp.Price; bk.Quantity=temp.Quantity;
	bk.PublishedDate=temp.PublishedDate;
	if err!=nil{
		panic(err)
	}
	DB.Save(&bk);
	writer.Write([]byte("Successfully updated"));
}
func DeleteBookById(writer http.ResponseWriter, request *http.Request) {
	var bk Book;
	id:=chi.URLParam(request,"id");
	DB.Unscoped().Where("ID=?",id).Delete(&bk)//.Unscoped();
	writer.Header().Set("Content-Type", "application/json")
	writer.Write([]byte("Successfully deleted"));
}
type DataProvider struct {
	DB *gorm.DB
}

func  Instantiate() *DataProvider{
	dbURI := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?parseTime=true", "root", "", "localhost", 3306, "swagger");
	gormConn, err := gorm.Open("mysql", dbURI)
	if err != nil {
		log.Print("Could not connect database")
	}
	DBMigrate(gormConn)
	return &DataProvider{gormConn}
}

type Book struct {
	ID uint  `gorm:"primaryKey"`
	BookName string
	BookAuthor string
	PublishedDate string
	Price float32
	Quantity int
}

func DBMigrate(db *gorm.DB) *gorm.DB {
	db.AutoMigrate(&Book{})
	return db
}
