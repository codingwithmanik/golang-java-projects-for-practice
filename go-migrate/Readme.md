1.create schema:
  a)Start with migrate create. Then the extension of the file will be sql, and the directory to store it is schemas.
  b)We use the -seq flag to generate a sequential version number for the migration file. And finally the name of 
    the migration, which is init_schema in this case.
  c)run: migrate create -ext sql -dir schemas -seq init_schema 
  d)we will get two file in schemas folder called 000001_init_schema.up.sql and 000001_init_schema.down.sql
2.run up.sql files to keeping up the database
  migrate -path schemas -database "postgresql://postgres:12345@localhost:5432/simple_bank?sslmode=disable" -verbose up
  note:We use -verbose option to ask migrate to print verbose logging.
3.see : https://dev.to/techschoolguru/how-to-write-run-database-migration-in-golang-5h6g