package main

import (
	"log"

	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/github"
)

func main() {
	m, err := migrate.New("mysql://localhost:3306/database?sslmode=enable")
	if err != nil {
		log.Fatal(err)
	}
	m.Steps(2)
}
