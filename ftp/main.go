
package main

import (
	"bytes"
	"fmt"
	"github.com/jlaffaye/ftp"
	"io/ioutil"
	"log"
	"regexp"
	"time"
)
func StrExtract(word string, wordToFind string) bool {
	r, _ := regexp.Compile(wordToFind)
	result := r.FindAllString(word, -1)
	if len(result) > 0 {
		return true
	}

	return false
}

func main() {
	//ftpfunc()
	fmt.Println(StrExtract("200", "x509"))
	fmt.Println(regexp.MatchString("[0-9][0-9][0-9]+", "403"))
	fmt.Println(regexp.MatchString("^Get https", "403"))
}

func ftpfunc() {
	c, err := ftp.Dial("192.168.0.103:21", ftp.DialWithTimeout(5*time.Second))
	if err != nil {
		log.Fatal(err)
	}
	err = c.Login("anonymous", "anonymous")
	if err != nil {
		log.Fatal(err)
	}
	//write operation
	content, err := ioutil.ReadFile("abc.xml")
	if err!=nil{
		log.Fatal(err)
	}
	data := bytes.NewBuffer(content)
	err = c.Stor("test-file.txt", data)
	if err != nil {
		panic(err)
	}
	//read operation
	r, err := c.Retr("test-file.txt")
	if err != nil {
		panic(err)
	}
	defer r.Close()
	buf, err := ioutil.ReadAll(r)
	println(string(buf))
	//quite your server connection
	if err := c.Quit(); err != nil {
		log.Fatal(err)
	}
}
