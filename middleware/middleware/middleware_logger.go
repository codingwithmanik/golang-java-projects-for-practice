package middleware

import (
	"fmt"
	"net/http"
	"time"
)

type Logger struct{}

func (l *Logger) ServeHTTP(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	fmt.Println("The logger middleware is executing!")
	t := time.Now()
	next.ServeHTTP(w, r) //we continue the execution of the next handler
	fmt.Printf("Execution time: %s \n", time.Now().Sub(t).String())
}
