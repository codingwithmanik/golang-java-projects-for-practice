### output of this program
    The logger middleware is executing!
    Endpoint handler called
    Execution time: 0s
- So first logger will start to execute 
- then by line 14 it will execute RouterHandler 
- after completing RouterHandler it will complete logger middleware.
- Me: so middlewares will be executed like chain(queue)
