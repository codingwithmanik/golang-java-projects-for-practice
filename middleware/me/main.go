package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
	"main/me/middleware"
	"net/http"
)

func main() {
	router := mux.NewRouter()
	router.
		Methods("GET").
		Path("/{name}").
		HandlerFunc(endpointHandler)

	router.Use(Printer) //we can read request data from this middleware
	n := negroni.New()
	n.Use(&middleware.Printer{}) // we can not read request data from this middleware.
	n.UseHandler(router)

	err := http.ListenAndServe(":8080", n)
	if err != nil {
		panic(err)
	}
}

func Printer(next http.Handler) http.Handler {
	abc := func(w http.ResponseWriter, r *http.Request) {
		params := mux.Vars(r)
		fmt.Println("From mux middleware: ", params["name"])
		if params["name"] != "Manik" {
			w.Write([]byte("Unauthorized"))
			w.WriteHeader(http.StatusUnauthorized)
			return
			//panic(errors.New("Unauthorized"))
		}
		next.ServeHTTP(w, r)
	}
	return http.HandlerFunc(abc)
}

func endpointHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Sohel Rana")
	w.Write([]byte("Endpoint handler called"))
}
