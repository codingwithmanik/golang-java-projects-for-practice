package middleware

import (
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
)

type Printer struct {
}

func (p *Printer) ServeHTTP(rw http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	params := mux.Vars(r)
	fmt.Println("From negroni middleware: ", params["name"])
	next.ServeHTTP(rw, r)
}
