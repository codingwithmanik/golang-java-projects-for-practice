package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
	"main/middleware"
	"net/http"
)

func main() {
	router := mux.NewRouter()
	router.
		Methods("GET").
		Path("/").
		HandlerFunc(endpointHandler)

	n := negroni.New()
	//the middleware are executed in the order they are inserted.
	n.Use(&middleware.Logger{})
	n.UseHandler(router)

	err := http.ListenAndServe(":8080", n)
	if err != nil {
		panic(err)
	}
}

func endpointHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Endpoint handler called")
	w.Write([]byte("Hello"))
}
