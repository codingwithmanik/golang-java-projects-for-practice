package data

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type BasicOperations interface {
	InsertOne(interface{}) (*mongo.InsertOneResult, error)
	InsertMany([]interface{}) (*mongo.InsertManyResult, error)
	FindAll(ctx context.Context, filter *bson.D) (*mongo.Cursor, error)
	FindOne(ctx context.Context, filter *bson.D) *mongo.SingleResult
	UpdateByID(ctx context.Context, ID interface{}, b bson.D) (*int64, error)
}

type dhandler struct {
	collection *mongo.Collection
}

func NewHandler(Dname, Cname string, client *mongo.Client) BasicOperations {
	return &dhandler{
		collection: client.Database(Dname).Collection(Cname),
	}
}

func (h *dhandler) InsertOne(data interface{}) (*mongo.InsertOneResult, error) {
	result, err := h.collection.InsertOne(context.TODO(), data)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (h *dhandler) InsertMany(data []interface{}) (*mongo.InsertManyResult, error) {
	results, err := h.collection.InsertMany(context.TODO(), data)
	if err != nil {
		return nil, err
	}
	return results, nil
}

func (h dhandler) FindAll(ctx context.Context, filter *bson.D) (*mongo.Cursor, error) {
	cursor, err := h.collection.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	return cursor, nil
}

func (h dhandler) FindOne(ctx context.Context, filter *bson.D) *mongo.SingleResult {
	return h.collection.FindOne(ctx, filter)
}

func (h *dhandler) UpdateByID(ctx context.Context, ID interface{}, updateQ bson.D) (*int64, error) {
	result, err := h.collection.UpdateByID(ctx, ID, updateQ)
	if err != nil {
		return nil, err
	}
	return &result.ModifiedCount, nil
}
