package data

import (
	"context"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"log"
)

func InitClient() *mongo.Client {
	client, err := mongo.Connect(context.TODO(), options.Client().ApplyURI("mongodb://localhost:27017"))
	if err != nil {
		log.Print("didn't able to connect with mongodb: ", err)
		panic(err)
	}
	if err := client.Ping(context.TODO(), readpref.Primary()); err != nil {
		log.Print("failed mongo connection: ", err)
		panic(err)
	}
	return client
}
