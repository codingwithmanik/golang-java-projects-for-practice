package model

import "go.mongodb.org/mongo-driver/bson/primitive"

type Employee struct {
	ID        primitive.ObjectID `bson:"_id,omitempty" json:"id"`
	FirstName string             `bson:"first_name" json:"first_name"`
	LastName  string             `bson:"last_name" json:"last_name"`
	Email     string             `bson:"email" json:"email"`
	Roll      int64              `bson:"roll" json:"roll"`
	Village   string             `bson:"village" json:"village"`
	Thana     string             `bson:"thana" json:"thana"`
	Zilla     string             `bson:"zilla" json:"zilla"`
}
