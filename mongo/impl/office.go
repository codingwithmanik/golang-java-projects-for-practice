package impl

import (
	"encoding/json"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"mongo-demo/data"
	"mongo-demo/impl/model"
	"net/http"
	"strconv"
)

const (
	OfficeDatabase   string = "office"
	OfficeCollection        = "employees"
)

type OfficeInterface interface {
	InsertEmployee(w http.ResponseWriter, r *http.Request)
	InsertEmployeeMany(w http.ResponseWriter, r *http.Request)
	FindAllEmployee(w http.ResponseWriter, r *http.Request)
	FindEmployee(w http.ResponseWriter, r *http.Request)
	UpdateEmployeeByID(w http.ResponseWriter, r *http.Request)
}

//oh, office handler
type oh struct {
	data data.BasicOperations
}

func NewOfficeHandler(c *mongo.Client) OfficeInterface {
	return &oh{
		data: data.NewHandler(OfficeDatabase, OfficeCollection, c),
	}
}

func (o *oh) InsertEmployee(w http.ResponseWriter, r *http.Request) {
	var emp model.Employee
	err := json.NewDecoder(r.Body).Decode(&emp)
	if err != nil {
		sendResponse(w, 502, "Error in InsertEmployee: "+err.Error())
		return
	}
	result, err := o.data.InsertOne(emp)
	if err != nil {
		sendResponse(w, 502, "Error in InsertEmployee: "+err.Error())
		return
	}
	sendResponse(w, 201, result)
}
func (o *oh) InsertEmployeeMany(w http.ResponseWriter, r *http.Request) {
	var body []interface{}
	err := json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		sendResponse(w, 502, "Error in FindAllEmployee: "+err.Error())
		return
	}
	results, err := o.data.InsertMany(body)
	if err != nil {
		sendResponse(w, 502, "Error in InsertEmployeeMany: "+err.Error())
		return
	}
	sendResponse(w, 201, results)
}

func (o *oh) FindAllEmployee(w http.ResponseWriter, r *http.Request) {
	filter := bson.D{
		{"$and",
			bson.A{
				bson.D{
					{"roll", bson.D{{"$gt", 13}}},
				},
			},
		},
	}
	ctx := r.Context()
	cursor, err := o.data.FindAll(ctx, &filter)
	if err != nil {
		sendResponse(w, 502, "Error in FindAllEmployee: "+err.Error())
		return
	}
	var emp []model.Employee
	err = cursor.All(ctx, &emp)
	if err != nil {
		sendResponse(w, 502, "Error in FindAllEmployee: "+err.Error())
		return
	}
	sendResponse(w, 200, emp)
}

func (o *oh) FindEmployee(w http.ResponseWriter, r *http.Request) {
	filter := bson.D{
		{"email", bson.D{{"$eq", "manik37343902@gmail.com"}}},
	}
	ctx := r.Context()
	var emp model.Employee
	err := o.data.FindOne(ctx, &filter).Decode(&emp)
	if err != nil {
		sendResponse(w, 404, "Error in FindEmployee: "+err.Error())
		return
	}
	sendResponse(w, 200, emp)
}

func (o *oh) UpdateEmployeeByID(w http.ResponseWriter, r *http.Request) {
	updateQ := bson.D{
		{"$set",
			bson.D{
				{"email", "manik@gmail.com"},
			},
		},
		{"$inc",
			bson.D{
				{"roll", 1},
			},
		},
	}
	ctx := r.Context()
	var id interface{} = "62aac2b2773b23b74ae28937"
	//id := bson.D{{"_id", "62aac2b2773b23b74ae28937"}}
	count, err := o.data.UpdateByID(ctx, id, updateQ)
	if err != nil {
		sendResponse(w, 502, "Error in UpdateEmployee: "+err.Error())
		return
	}
	sendResponse(w, 200, "Total updated docs: "+strconv.FormatInt(*count, 10))
}

func sendResponse(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}
