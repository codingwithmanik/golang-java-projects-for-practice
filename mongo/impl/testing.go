package impl

import (
	"go.mongodb.org/mongo-driver/mongo"
	"mongo-demo/data"
)

const (
	TestDatabase   string = "testing"
	TestCollection        = "users"
)

type TestingInterface interface {
	//has no operation of database testing
}

//th, testing handler
type th struct {
	data data.BasicOperations
}

func NewTestingHandler(client *mongo.Client) TestingInterface {
	return &th{
		data: data.NewHandler(TestDatabase, TestCollection, client),
	}
}
