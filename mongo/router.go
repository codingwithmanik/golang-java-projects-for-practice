package main

import (
	"github.com/go-chi/chi"
	"mongo-demo/impl"
)

func Router(o impl.OfficeInterface, t impl.TestingInterface) *chi.Mux {
	router := chi.NewRouter()
	router.Post("/insert-employee", o.InsertEmployee)
	router.Post("/insert-employee-many", o.InsertEmployeeMany)
	router.Get("/filter-employee", o.FindAllEmployee)
	router.Get("/filter-particular-employee", o.FindEmployee)
	router.Put("/update-an-employee", o.UpdateEmployeeByID)
	return router
}
