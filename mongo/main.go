package main

import (
	"log"
	"mongo-demo/data"
	"mongo-demo/impl"
	"net/http"
)

func main() {
	client := data.InitClient()
	oh := impl.NewOfficeHandler(client)
	th := impl.NewTestingHandler(client)
	router := Router(oh, th)
	log.Println("Your server is listening at port 8080")
	err := http.ListenAndServe(":8080", router)
	if err != nil {
		log.Println("Failed to start server with error.types: ", err.Error())
	}
}
