### Compare with Database
* Database----> Database 
* Table-------> Collection
* Row/Record--> Document

### This demo app inspired from
[here](https://blog.logrocket.com/how-to-use-mongodb-with-go/)

### Mongodb query
* see [here](https://www.mongodb.com/docs/manual/reference/operator/query/)
* whole [manual](https://www.mongodb.com/docs/manual/)