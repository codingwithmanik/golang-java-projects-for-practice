package main

/*
type movie struct {
	name string
	year int32
	cast []actor
}
type actor struct {
	name string
	role string
}

func (m movie) String() string {
	return fmt.Sprintf("%v (%v)\n%v", m.name, m.year, m.cast)
}

func (a actor) String() string {
	return fmt.Sprintf("%v: %v", a.name, a.role)
}

func main() {
	mv := movie{
		name: "Blade Runner",
		year: 1982,
		cast: []actor{
			{name: "Harrison", role: "Rick Deckard"},
			{name: "Sean Young", role: "Rachel Tyrell"},
			{name: "Rutger Hauer", role: "Roy Baty"},
		},
	}
	fmt.Println(mv)
}
*/
