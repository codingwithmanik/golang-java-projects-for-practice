package main

import (
	"fmt"
	"strings"
)

type movie struct {
	name string
	year int32
	cast caststruct
}

type caststruct []actor

type actor struct {
	name string
	role string
}

func (m movie) String() string {
	return fmt.Sprintf("%v (%v)\n%v", m.name, m.year, m.cast)
}

func (cc caststruct) String() string {
	var caststrings []string
	for _, castmember := range cc {
		caststrings = append(caststrings, castmember.String())
	}
	cast := strings.Join(caststrings, "\n")
	return cast
}
func (m actor) String() string {
	return fmt.Sprintf("%v: %v", m.name, m.role)
}

func main() {
	mv := movie{
		name: "Blade Runner",
		year: 1982,
		cast: []actor{
			{name: "Harrison", role: "Rick Deckard"},
			{name: "Sean Young", role: "Rachel Tyrell"},
			{name: "Rutger Hauer", role: "Roy Baty"},
		},
	}
	fmt.Println(mv)

}
