package main

import "fmt"

//go:generate stringer -type=University,Role,Member main2.go
type University struct {
	name     string
	location string
	members  []Member
}

type Role int

const (
	Admin Role = iota
	Teacher
	Student
)

type Member struct {
	name  string
	roles Role
}

func main() {
	u := University{name: "BUBT", location: "Mirpur-2, Dhaka",
		members: []Member{
			{name: "Manik", roles: Student},
			{name: "Ashikur", roles: Student},
			{name: "Saifur Rahman Dipu", roles: Teacher},
		},
	}
	fmt.Println(u.members[0].roles)
	a := Student
	fmt.Println(a)
}
