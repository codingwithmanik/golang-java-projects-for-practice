## Setup
- Install stringer ```go get -u golang.org/x/tools/cmd/stringer```
## Usecases
- We can console our struct value in a nice way
- And for enum it will provide <b>String</b> instead of its <b>Ordinal</b> value