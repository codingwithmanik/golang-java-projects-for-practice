module github.com/Joule-CMA/GoLang-Services-Template

go 1.15

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/go-chi/render v1.0.1
	github.com/google/uuid v1.1.1 // indirect
	github.com/jinzhu/gorm v1.9.11
	github.com/rs/zerolog v1.15.0
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
)
