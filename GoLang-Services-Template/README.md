# GoLang-Services-Template

## Steps

1. Rename service.core directory under the cmd to your service<Actually, do we just leave it as service.core?>
2. 

## TODO 

1. 'GetHostIP, GetPublicIP, setZeroLog' in cmd/service.core/main.go: what are they used for and should we move this in a library
2. 'router.Use(middleware.Logger) // modify to use stackdriver' in cmd/service.core/main.go: This is copied, but should it be modified or old comment?
3. 'router setup': should it be in a library to keep it consistent across the services?
4. Helper function in library in order to create the zerolog logger with standard configuration. 
5. Catch all for the poison queue logging
6. Move the database setup functions into a library for reuse
