package services

import (
	"github.com/Joule-CMA/GoLang-Services-Template/pkg/Listener/HelloListener/definitions"
	"github.com/rs/zerolog"
)

type HelloListenerServiceConfig struct {
	definitions.HelloListenerProviderInterface
	Log			*zerolog.Logger
}

// ImplementedBy, creates a configuration object from the definition
func ImplementedBy(implementation definitions.HelloListenerProviderInterface, componentLogger *zerolog.Logger) HelloListenerServiceConfig {
	return HelloListenerServiceConfig{
		implementation,
		componentLogger,
	}
}
