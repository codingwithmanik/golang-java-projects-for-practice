package services

import (
	"encoding/json"
	"github.com/Joule-CMA/Error"
	"github.com/Joule-CMA/GoLang-Services-Template/pkg/Model"
	PubSub "github.com/Joule-CMA/PubSub"
	"strings"
)

func (s *HelloListenerServiceConfig) ProcessHelloWorld(_ string, payload string, _ *PubSub.PoisonInfo) error {
	// Unmarshal object of HelloWorldModel
	var helloWorld Model.HelloWorldModel
	s.Log.Info().Msg("HelloListener:ProcessHelloWorld::Check if the pubsub object contains a HelloWorld object")
	err := json.NewDecoder(strings.NewReader(payload)).Decode(&helloWorld)

	// Check to see if there was a problem unmarshalling our response
	if err != nil || (helloWorld.HelloTitle == "") {
		//Error unmarshaling data
		if err != nil {
			s.Log.Error().Err(err).Msg("HelloListener:ProcessHelloWorld::Unable to marshal body")
		}
		s.Log.Error().Msg("HelloListener:ProcessHelloWorld::Update object not in body")
		jsonError := Error.JsonError{Message: "Update object not in body", ErrorCode: "hello.world.json.marshal.error.types"}
		return &jsonError
	}

	err = s.SaveHelloWorld(helloWorld)

	// Check if the error.types was caught saving the object
	if err != nil {
		s.Log.Error().Err(err).Msg("HelloListener:ProcessHelloWorld::Failed to save college data in CRM")
		jsonError := Error.JsonError{Message: "Failed to save data in database", ErrorCode: "hello.world.save.failed"}
		return &jsonError
	}

	return nil
}
