package HelloListener

import (
	"github.com/Joule-CMA/GoLang-Services-Template/pkg/DataProvider"
	"github.com/Joule-CMA/GoLang-Services-Template/pkg/Listener/HelloListener/implementations"
	"github.com/Joule-CMA/GoLang-Services-Template/pkg/Listener/HelloListener/services"
	PubSub "github.com/Joule-CMA/PubSub"
	"github.com/rs/zerolog"
)

type HelloListenerComponent struct {
	DataProvider *DataProvider.HelloListenerDatabase
}

func (s *HelloListenerComponent) StartListener() {
	// Set time format of logs
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix

	// Get a new implementation of the Member Claim Configuration
	implementation := implementations.Configure(s.DataProvider)
	implementation.Log.Info().Msg("HelloListenerComponent: Initializing configuration")

	// Tie the implementation with the services (each services is business use case)
	implementation.Log.Info().Msg("HelloListenerComponent: Configuration added to the implementation")
	claimListenerConfiguration := services.ImplementedBy(implementation,&implementation.Log)

	// Start the router for the subscriber
	implementation.Log.Info().Msg("HelloListenerComponent: Starting the HelloWorld subscriber")
	PubSub.StartRouter(
		"hello_world",
		"hello.world.handler",
		"HELLO.WORLD",
		claimListenerConfiguration.ProcessHelloWorld,
		"ERROR.HELLO.WORLD")
	x := ""
	x = x
}
