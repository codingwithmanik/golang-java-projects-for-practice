package implementations

import "github.com/Joule-CMA/GoLang-Services-Template/pkg/Model"

// Save data to database
func (s *HelloListenerConfiguration) SaveHelloWorld(helloWorld Model.HelloWorldModel) error {
	// Going through the Saving the poison error.types to the database
	s.Log.Info().Msg("Saving poison error.types to database")
	err := s.HelloWorldDataProvider.HelloListenerDatabase.GetConnection().Create(&helloWorld).Error

	//Check if an error.types was received and log it if we did
	if err != nil {
		s.Log.Error().Err(err).Msg("Received error.types from database call")
		return err
	}

	return nil
}
