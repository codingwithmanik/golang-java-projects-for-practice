package implementations

import (
	"github.com/Joule-CMA/GoLang-Services-Template/pkg/DataProvider"
	"github.com/Joule-CMA/GoLang-Services-Template/pkg/Model"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"os"
)

//Configuration items for the Hello World Listener package
type HelloListenerConfiguration struct {
	Log			zerolog.Logger
	HelloWorldDataProvider *DataProvider.HelloListenerDatabase
}

// Configuration for the original survey implementation.  It will create the database configuration
func Configure(DataProvider *DataProvider.HelloListenerDatabase) *HelloListenerConfiguration {
	// Create configuration object
	var configuration HelloListenerConfiguration

	// Set dataProvider
	configuration.Log.Info().Msg("HelloWorldConfiguration: setting the data provider")
	configuration.HelloWorldDataProvider = DataProvider

	// Set the output of the logger of zerolog
	configuration.Log = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})

	//Set the component of zerolog for this package
	configuration.Log = log.Logger.With().Str("component", "HelloListener").Logger()
	configuration.Log.Info().Msg("HelloWorldConfiguration: Component value set for HelloListener logger")

	// Auto migrate dependant tables
	configuration.Log.Info().Msg("HelloWorldConfiguration: Auto migrating database tables")
	configuration.HelloWorldDataProvider.HelloListenerDatabase.AutoMigrate(&Model.HelloWorldModel{})

	// Check to make sure that the tables in the database are correct
	if (!configuration.HelloWorldDataProvider.HelloListenerDatabase.GetConnection().HasTable(&Model.HelloWorldModel{})) {
		panic("HelloWorld table does not exist")
	}
	configuration.Log.Info().Msg("HelloWorldConfiguration: Profile table exists")

	// Return configuration object created
	configuration.Log.Info().Msg("Implementation configuration object created")
	return &configuration
}
