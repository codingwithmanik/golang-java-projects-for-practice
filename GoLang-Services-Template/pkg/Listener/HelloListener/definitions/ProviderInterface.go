package definitions

import "github.com/Joule-CMA/GoLang-Services-Template/pkg/Model"

// this is the contract that allows us to bind the business logic (use cases) in the component package
// to the implementation in the implementationOf package
type HelloListenerProviderInterface interface {

	// *********************************************************
	// found in implementations/SaveHelloWorld.go
	// *********************************************************
	SaveHelloWorld(poisonError Model.HelloWorldModel) error
}

