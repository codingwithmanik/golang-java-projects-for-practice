module bulk-insert

go 1.14

require (
	github.com/jinzhu/gorm v1.9.16
	github.com/thoas/go-funk v0.9.0
)
