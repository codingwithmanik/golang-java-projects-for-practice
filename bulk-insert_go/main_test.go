package main

import (
	"fmt"
	"math/rand"
	"strings"
	"testing"

	"github.com/jinzhu/gorm"
	"github.com/thoas/go-funk"
)

//https://betterprogramming.pub/how-to-bulk-create-and-update-the-right-way-in-golang-part-i-e15a8e5585d1
//second part(update operation) is not available yet.
const letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

func genRandomString(length int) string {
	b := make([]byte, length)

	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}

	return string(b)
}

func stubUsers(b *testing.B) (users []*User) {
	fmt.Println("N= ", b.N)
	for i := 0; i < b.N; i++ {
		user := &User{
			Name:     genRandomString(100),
			Password: genRandomString(100),
		}
		users = append(users, user)
	}
	return users
}

//1. Insert multiple records using GORM
func BenchmarkOrmCreate(b *testing.B) {
	db, err := gorm.Open("postgres", "host=localhost port=5432 user=postgres dbname=testdb password=12345 sslmode=disable")
	if err != nil {
		fmt.Println(err)
	}
	defer db.Close()

	users := stubUsers(b)
	for _, user := range users {
		db.Create(user)
	}
}

//2. Insert multiple records by natively using the INSERT statement
func BenchmarkCreate(b *testing.B) {
	db, err := gorm.Open("postgres", "host=localhost port=5432 user=postgres dbname=testdb password=12345 sslmode=disable")
	if err != nil {
		fmt.Println(err)
	}
	defer db.Close()

	users := stubUsers(b)
	tx := db.Begin()
	valueStrings := []string{}
	valueArgs := []interface{}{}
	for _, user := range users {
		valueStrings = append(valueStrings, "(?, ?)")
		valueArgs = append(valueArgs, user.Name)
		valueArgs = append(valueArgs, user.Password)
	}

	stmt := fmt.Sprintf("INSERT INTO users (name, password) VALUES %s", strings.Join(valueStrings, ","))
	err = tx.Exec(stmt, valueArgs...).Error
	if err != nil {
		tx.Rollback()
		fmt.Println(err)
	}
	err = tx.Commit().Error
	if err != nil {
		fmt.Println(err)
	}
}

//3. Insert multiple records using the INSERT statement with batching size
func BenchmarkBulkCreate(b *testing.B) {
	db, err := gorm.Open("postgres", "host=localhost port=5432 user=postgres dbname=testdb password=12345 sslmode=disable")
	if err != nil {
		fmt.Println(err)
	}
	defer db.Close()

	users := stubUsers(b)
	size := 500
	tx := db.Begin()
	chunkList := funk.Chunk(users, size)
	for _, chunk := range chunkList.([][]*User) {
		valueStrings := []string{}
		valueArgs := []interface{}{}
		for _, user := range chunk {
			valueStrings = append(valueStrings, "(?, ?)")
			valueArgs = append(valueArgs, user.Name)
			valueArgs = append(valueArgs, user.Password)
		}

		stmt := fmt.Sprintf("INSERT INTO users (name, password) VALUES %s", strings.Join(valueStrings, ","))
		err = tx.Exec(stmt, valueArgs...).Error
		if err != nil {
			tx.Rollback()
			fmt.Println(err)
		}
	}
	err = tx.Commit().Error
	if err != nil {
		fmt.Println(err)
	}
}

//4. Insert multiple records using the INSERT statement with dynamic batching size
func benchmarkBulkCreate(size int, b *testing.B) {
	db, err := gorm.Open("postgres", "host=localhost port=5432 user=postgres dbname=testdb password=12345 sslmode=disable")
	if err != nil {
		fmt.Println(err)
	}
	defer db.Close()

	users := stubUsers(b)
	tx := db.Begin()
	chunkList := funk.Chunk(users, size)
	for _, chunk := range chunkList.([][]*User) {
		valueStrings := []string{}
		valueArgs := []interface{}{}
		for _, user := range chunk {
			valueStrings = append(valueStrings, "(?, ?)")
			valueArgs = append(valueArgs, user.Name)
			valueArgs = append(valueArgs, user.Password)
		}

		stmt := fmt.Sprintf("INSERT INTO users (name, password) VALUES %s", strings.Join(valueStrings, ","))
		err = tx.Exec(stmt, valueArgs...).Error
		if err != nil {
			tx.Rollback()
			fmt.Println(err)
		}
	}
	err = tx.Commit().Error
	if err != nil {
		fmt.Println(err)
	}
}
func BenchmarkBulkCreateSize1(b *testing.B) {
	benchmarkBulkCreate(1, b)
}
func BenchmarkBulkCreateSize100(b *testing.B) {
	benchmarkBulkCreate(100, b)
}

//perfect to use. got 260022 parameters but PostgreSQL only supports 65535
func BenchmarkBulkCreateSize500(b *testing.B) {
	benchmarkBulkCreate(500, b)
}

func BenchmarkBulkCreateSize1000(b *testing.B) {
	benchmarkBulkCreate(1000, b)
}
