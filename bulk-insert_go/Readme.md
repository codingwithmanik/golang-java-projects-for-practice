We have 4 approaches:
1. Insert multiple records using GORM
      685773 ns/op(my pc),5494541 ns/op(bloger pc)(nonosecond/per loop)

2. Insert multiple records by natively using the INSERT statement
     a)8944 ns/op(my pc), 8354 ns/op(bloger pc) but the problem is
     b)when N=45552 then the total parameters(name,address) become 91104 then server will throw: got 91104 
       parameters but PostgreSQL only supports 65535 parameters
     c)So the solution is using batch system

3. Insert multiple records using the INSERT statement with batching size
     a)23365ns/op(bloger pc), 23017ns/op(my pc)
     b)OK, it might perform a little bit worse than the second approach but now it guarantees consistency when 
       inserting new records, which is the most important thing you must think about first.
       note: Here we are using batch size 500.
     c)Right now, you should have some questions like: “What happens when we make the batch size dynamically 
       or insert more columns?” We get to the last approach(follow approach no 4).

4. Insert multiple records using the INSERT statement with dynamic batching size
     a)In this approach you will get speed like approach 3, but here we are using dynamic batch size.

***ThreeStartNote: Now gorm also providing bulk-insert from version v:2.0
     a)https://gorm.io/docs/v2_release_note.html
     b)https://github.com/t-tiger/gorm-bulk-insert    

Sources: https://betterprogramming.pub/how-to-bulk-create-and-update-the-right-way-in-golang-part-i-e15a8e5585d1