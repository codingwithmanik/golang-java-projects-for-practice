package main

import (
	"fmt"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

func main() {
	db, err := gorm.Open("postgres", "host=localhost port=5432 user=postgres dbname=testdb password=12345 sslmode=disable")
	if err != nil {
		fmt.Println("Could not able to connect database:", err)
		return
	}
	defer db.Close()
	db.AutoMigrate(&User{})
}

type User struct {
	gorm.Model
	Name     string
	Password string
}
