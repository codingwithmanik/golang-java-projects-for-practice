package main

import "net/http"

func main() {
	http.HandleFunc("/hello", h)
	http.ListenAndServe(":8080", nil)
}
func h(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Hello Message"))
}
