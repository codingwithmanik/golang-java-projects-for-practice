package main

import (
	"fmt"
	"github.com/golang/protobuf/proto"
	"io/ioutil"
	"log"
	bufferfile "protocol-buffer-2/bufferfiles"
)


//testing with package(bufferfiles,protofiles) files.
func main(){

	//p := []*bufferfile.Person{
	//	{
	//		Id:    1234,
	//		Name:  "John Doe",
	//		Email: "jdoe@example.com",
	//		Phones: []*bufferfile.Person_PhoneNumber{
	//			{Number: "555-4321", Type: bufferfile.Person_HOME},
	//		},
	//	},
	//}
	//ab := &bufferfile.AddressBook{
	//	People:p,
	//}

	//out, err := proto.Marshal(ab)
	//if err != nil {
	//	log.Fatalln("Failed to encode address book:", err)
	//}

	/*The whole purpose of using protocol buffers is to serialize your data so that it can be parsed elsewhere.
	  In Go, you use the proto library's Marshal function to serialize your protocol buffer data. A pointer to
	  a protocol buffer message's struct implements the proto.Message interface. Calling proto.Marshal returns
	  the protocol buffer, encoded in its wire format. */

	//if err := ioutil.WriteFile("protofiles/addressbook.txt", out, 0777); err != nil {
	//	log.Fatalln("Failed to write address book:", err)
	//}

	/*To parse an encoded message, you use the proto library's Unmarshal function. Calling this parses the data
	  in buf as a protocol buffer and places the result in pb.*/
	// Read the existing address book.
	in, err := ioutil.ReadFile("protofiles/addressbook.txt")
	if err != nil {
		log.Fatalln("Error reading file:", err)
	}
	book := &bufferfile.AddressBook{}
	if err := proto.Unmarshal(in, book); err != nil {
		log.Fatalln("Failed to parse address book:", err)
	}
	fmt.Println(book.People)
}
