package main

import (
	"fmt"
	"github.com/golang/protobuf/proto"
	"google.golang.org/protobuf/types/known/timestamppb"
	"log"
)

// testing with project directory files.
func main() {
	fmt.Println(timestamppb.Now())
	ashikur := &Person{
		Name: "Ashikur",
		Age:  22,
		SocialFollowers: &SocialFollowers{
			Youtube: 2500,
			Twitter: 1400,
		},
	}

	data, err := proto.Marshal(ashikur)
	if err != nil {
		log.Fatalln("Mashing error.types", err)
	}

	newAshikur := &Person{}
	err = proto.Unmarshal(data, newAshikur)

	if err != nil {
		log.Fatalln("Unmassing error.types: ", err)
	}

	//newAshikur.Reset()
	fmt.Println(newAshikur.GetName())
	fmt.Println(newAshikur.GetAge())
	fmt.Println(newAshikur.SocialFollowers.GetTwitter())
	fmt.Println(newAshikur.SocialFollowers.GetYoutube())
}
