package main

import (
	"bitbucket.org/qsbbackendteam/calc1/proto"
	"context"
	"fmt"
	"github.com/micro/go-micro/v2"
)

func main(){
	service := micro.NewService(
		micro.Name("client"), //name the client service
	)
	// Initialise service
	service.Init()
	// Create new greeter client
	client1 := proto.NewServiceProviderService("calc1", service.Client())
    resp,err:=client1.Add(context.TODO(),&proto.SumRequest{A: 5,B: 6})
    if err!=nil{
    	panic(err)
	}
	fmt.Println("The result is(Add): ",resp.Result)
	res,er:=client1.Multiply(context.TODO(),&proto.MulRequest{A: 5,B: 6})
	if er!=nil{
		panic(err)
	}
	fmt.Println("The result is(Multiply): ",res.Result)
    //errr := service.Run()
    //if errr!=nil{
    //	panic(errr)
	//}


   /*
	client2 := serv2.NewServiceProviderService("calc2",client.DefaultClient)
	// request the Add method on the ServiceProvider handler
	res,err:= client2.Sub(context.TODO(),&serv2.CalcRequest{
		A: 10,
		B: 6,
	})
	if err!=nil{
		fmt.Println(err)
		return
	}
	fmt.Println("The result is(Sub):", res.Result)  */
}