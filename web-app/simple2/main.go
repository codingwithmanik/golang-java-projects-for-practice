package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
)

type ToDo struct {
	Item string
	Done bool
}
type PageData struct {
	Title string
	Todos []ToDo
}

var tmpl *template.Template

func main() {
	mux := http.NewServeMux()
	tmpl = template.Must(template.ParseFiles("templates/index.html"))

	fs := http.FileServer(http.Dir("./static"))
	mux.Handle("/static/", http.StripPrefix("/static/", fs))

	mux.HandleFunc("/todo", todo)
	fmt.Println("Your server listening at localhost:8080")
	log.Fatal(http.ListenAndServe(":8080", mux))
}

func todo(writer http.ResponseWriter, request *http.Request) {
	data := PageData{
		Title: "TODO List",
		Todos: []ToDo{
			{Item: "Install Go", Done: true},
			{Item: "Learn Go", Done: false},
			{Item: "Like this vibe", Done: false},
		},
	}
	tmpl.Execute(writer, data)
}
