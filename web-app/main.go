package main

import "fmt"

func main() {
	a := &Error{Code: 401}
	if c := a.GetCode(); c == 401 {
		fmt.Println("400")
		fmt.Println(&a.Code, "  ", &c)
	} else {
		fmt.Println("its okay")
	}
}

type Error struct {
	Code int32
}

func (e *Error) GetCode() int32 {
	return e.Code
}
