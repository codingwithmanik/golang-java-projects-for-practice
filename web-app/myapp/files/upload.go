package files

import (
	"encoding/csv"
	"errors"
	"fmt"
	"net/http"
	"webapp/myapp/model"
)

func Upload(request *http.Request) ([]*model.Registration, error) {
	//20<<20=20mb; 30<<20=30mb; 50<<20=50mb
	err := request.ParseMultipartForm(10 << 20) //10 mb
	if err != nil {
		return nil, err
	}
	file, handler, err := request.FormFile("myFile")
	if err != nil {
		return nil, err
	}
	defer file.Close()

	fmt.Printf("Uploaded File: %+v\n", handler.Filename)
	fmt.Printf("File Size: %+v\n", handler.Size)
	//fmt.Printf("MIME Header: %+v\n", handler.Header)
	if handler.Filename == "" || handler.Size <= 0 {
		return nil, errors.New("empty file")
	}

	//Save file section commented out
	/*
		// Create a temporary file within our temp-images directory that follows
		// a particular naming pattern
		tempFile, err := ioutil.TempFile("./files", "upload-*.csv")
		if err != nil {
			return nil, err
		}
		defer tempFile.Close()
		// read all of the contents of our uploaded file into a
		// byte array
		fileBytes, err := ioutil.ReadAll(file)
		if err != nil {
			fmt.Println(err)
		}
		// write this byte array to our temporary file
		_, err = tempFile.Write(fileBytes)
		if err != nil {
			return nil, err
		}
	*/

	//read file data
	var r []*model.Registration
	csvLines, err := csv.NewReader(file).ReadAll()
	if err != nil {
		return nil, err
	}
	for _, line := range csvLines {
		reg := &model.Registration{
			FirstName: line[0],
			LastName:  line[1],
			Phone:     line[2],
			Username:  line[2],
			Email:     line[2],
			Password:  line[2],
		}
		r = append(r, reg)
	}
	return r, nil
}
