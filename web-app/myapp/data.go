package main

import (
	"net/http"
	"webapp/myapp/model"
)

func userLogin(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		tmpl.Execute(w, nil)
		return
	}
	l := &model.Login{}
	l.Username = r.FormValue("username")
	l.Password = r.FormValue("password")
	if l.Username == "manik" && l.Password == "manik" {
		setLogin(w, r)
	} else {
		http.Redirect(w, r, "/login", http.StatusFound)
	}
}

func saveRegistration(writer http.ResponseWriter, req *http.Request) {
	if req.Method != http.MethodPost {
		tmpl.Execute(writer, nil)
		return
	}
	r := &model.Registration{}
	r.FirstName = req.FormValue("firstname")
	r.LastName = req.FormValue("lastname")
	r.Phone = req.FormValue("phone")
	r.Username = req.FormValue("username")
	r.Email = req.FormValue("email")
	r.Password = req.FormValue("password")
	if len(r.Email) > 0 {
		http.Redirect(writer, req, "/login", http.StatusFound)
	} else {
		http.Redirect(writer, req, "/registration", http.StatusFound)
	}
}

func setLogin(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, "cookie-name")

	// Authentication goes here
	// ...

	// Set user as authenticated
	session.Values["authenticated"] = true
	session.Save(r, w)
	http.Redirect(w, r, "/dashboard", http.StatusFound)
}

func getLogin(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, "cookie-name")

	// Check if user is authenticated
	if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
		http.Redirect(w, r, "/login", http.StatusFound)
	}
}

func logout(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, "cookie-name")
	// Revoke users authentication
	session.Values["authenticated"] = false
	session.Save(r, w)
}
