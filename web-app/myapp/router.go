package main

import (
	"fmt"
	"net/http"
	"webapp/myapp/files"
	"webapp/myapp/model"
)

func registration(writer http.ResponseWriter, request *http.Request) {
	err := tmpl.ExecuteTemplate(writer, "registration.html", nil)
	if err != nil {
		fmt.Println(err)
	}
}

func login(writer http.ResponseWriter, request *http.Request) {
	err := tmpl.ExecuteTemplate(writer, "login.html", nil)
	if err != nil {
		fmt.Println(err)
	}
}

func dashboard(w http.ResponseWriter, r *http.Request) {
	getLogin(w, r)
	var RData []*model.Registration
	RData, err := files.Upload(r)
	if err != nil || len(RData) == 0 {
		fmt.Println("Static data loaded(got error.types or empty file)-> ", err)
		RData = []*model.Registration{
			{"Manik", "Hossain", "01473265821", "manik", "manik@gmail.com", ""},
			{"Ashikur", "Rahman", "01758652341", "rashid", "rashid@gmail.com", ""},
			{"Chayan", "Roy", "01823659874", "learner", "learner@gmail.com", ""},
			{"Shankar", "Devsharma", "01845132844", "shankar", "dev100@gmail.com", ""},
			{"Sohel", "Rana", "01845454135", "rana", "rana000@gmail.com", ""},
			{"Manna", "Islam", "01996139108", "manna", "hero@gmail.com", ""},
		}
	}
	data := model.DashBoardData{
		RegistrationList: RData,
	}
	err = tmpl.ExecuteTemplate(w, "dashboard.html", data)
	if err != nil {
		fmt.Println(err)
	}
}
