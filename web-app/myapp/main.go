package main

import (
	"fmt"
	"github.com/gorilla/sessions"
	"html/template"
	"log"
	"net/http"
)

var (
	// key must be 16, 24 or 32 bytes long (AES-128, AES-192 or AES-256)
	key   = []byte("super-secret-key")
	store = sessions.NewCookieStore(key)
	tmpl  *template.Template
)

func main() {
	mux := http.NewServeMux()
	tmpl = template.Must(template.ParseFiles("templates/registration.html", "templates/login.html",
		"templates/dashboard.html"))

	fs := http.FileServer(http.Dir("./static"))
	mux.Handle("/static/", http.StripPrefix("/static/", fs))

	mux.HandleFunc("/registration", logging(registration))
	mux.HandleFunc("/login", logging(login))
	mux.HandleFunc("/dashboard", logging(dashboard))
	//============================DATA===============================
	mux.HandleFunc("/saveRegistration", saveRegistration)
	mux.HandleFunc("/userLogin", userLogin)

	fmt.Println("Your server listening at localhost:8080")
	log.Fatal(http.ListenAndServe(":8080", mux))
}

func logging(f http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		log.Println(r.URL.Path)
		f(w, r)
	}
}
