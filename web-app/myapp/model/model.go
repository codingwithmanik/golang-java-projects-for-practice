package model

type Registration struct {
	FirstName string
	LastName  string
	Phone     string
	Username  string
	Email     string
	Password  string
}
type Login struct {
	Username string
	Password string
}

type DashBoardData struct {
	RegistrationList []*Registration
}
