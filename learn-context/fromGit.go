package main

import (
	"bufio"
	"context"
	"fmt"
	"log"
	"os"
	"time"
)

func withTimeOut() {
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, time.Second)
	defer cancel()

	sleepAdTalk(ctx, 5*time.Second, "Hello withTimeOut!")
}

func withCancel() {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)

	go func() {
		s := bufio.NewScanner(os.Stdin)
		s.Scan()
		cancel()
	}()

	sleepAdTalk(ctx, 5*time.Second, "Hello withCancel!")
}

func background() {
	ctx := context.Background()
	sleepAdTalk(ctx, 5*time.Second, "Hello background!")
}

func sleepAdTalk(ctx context.Context, d time.Duration, s string) {
	select {
	case <-time.After(d):
		fmt.Println(s)
	case <-ctx.Done():
		log.Println(ctx.Err())
	}
}

func main() {
	background()
	withCancel()
	withTimeOut()
}
