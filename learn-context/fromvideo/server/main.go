package main

import (
	"fmt"
	"learncontext/fromvideo/log"
	"net/http"
	"time"
)

func main(){
	http.HandleFunc("/",log.Decorate(handler))
	fmt.Println("Your server is listening on port 8080")
	panic(http.ListenAndServe("127.0.0.1:8080",nil))
}

func handler(writer http.ResponseWriter, request *http.Request) {
	ctx := request.Context()
	log.Println(ctx, "handler started")
	defer log.Println(ctx, "handler ended")
	select {
		case <- time.After(5 * time.Second) :
			fmt.Fprintln(writer,"Hello")
		case <- ctx.Done() :
			err := ctx.Err()
			log.Println(ctx, err.Error())
			http.Error(writer,err.Error(),http.StatusInternalServerError)
	}

}
