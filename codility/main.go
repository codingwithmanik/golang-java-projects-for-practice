package main

import "fmt"

func main() {

	var a int
	var b *int
	var c **int
	a = 25
	fmt.Println(a, " ", &a)
	b = &a
	fmt.Println(b, " ", *b, " ", &b)
	c = &b
	fmt.Println(c, " ", *c, " ", &c, " ", **c)
	d := &c
	fmt.Println(d, " ", *d, " ", &d, " ", **d, " ", ***d)
}
