package main

import "fmt"

func main() {
	a := []int{1, 2}
	b := []int{3, 4}
	check := a
	copy(b, a)
	c := []int{5, 6}
	copy(a, c)
	fmt.Println(a, b, check)
}
