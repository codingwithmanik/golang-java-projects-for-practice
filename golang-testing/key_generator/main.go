package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {
	op := GetKeyOperator()
	key := op.Generate(2, 3)
	a, b, _ := op.Degenerate(key)
	fmt.Printf("key=%v, a=%v, b=%v", key, a, b)
}

type keyOp struct {
	Template string
}

func GetKeyOperator() *keyOp {
	op := &keyOp{
		Template: "%v_%v",
	}
	return op
}

func (kp *keyOp) Generate(x, y int) string {
	return fmt.Sprintf(kp.Template, x, y)
}

func (kp *keyOp) Degenerate(s string) (int, int, error) {
	if idx := strings.IndexByte(s, '_'); idx >= 0 {
		before, _ := strconv.Atoi(s[:idx])
		after, _ := strconv.Atoi(s[idx+1:])

		return before, after, nil
	} else {
		return 0, 0, fmt.Errorf("not_a_valid_key")
	}
}
