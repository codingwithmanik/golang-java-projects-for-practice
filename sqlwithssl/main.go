package main

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	gormProxy "github.com/go-sql-driver/mysql"
	"gorm.io/driver/mysql"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"log"
	"os"
)

func main() {
	//Postgres
	connectPostgres()
	//Mysql
	connectMysql()
}

func connectPostgres() {
	dsn := "sslmode=verify-ca sslrootcert=server-ca.pem sslcert=client-cert.pem sslkey=client-key.pem host=34.131.192.34 user=postgres password=,]J0Lu3M;uhS924d dbname=postgres port=5432 "
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Error),
	})
	if err != nil {
		panic("Got error: " + err.Error())
	}
	fmt.Println("Postgres Connected: ", db)
}

func connectMysql() {
	tlsConfig := createTLSConf()
	customConfig := mysql.Config{
		DSN: "manik:Lk{]F5+Rs&hI6'Jj@tcp(34.122.84.175:3306)/university?tls=custom&charset=utf8mb4&parseTime=True&loc=Local",
	}
	err := gormProxy.RegisterTLSConfig("custom", &tlsConfig)
	if err != nil {
		panic("Failed to regis tls: " + err.Error())
	}
	// Create a new Dialector with the Config.
	dialector := mysql.New(customConfig)
	db, err := gorm.Open(dialector, &gorm.Config{})
	if err != nil {
		panic(err.Error())
	}
	fmt.Println("Mysql Connected: ", db)
}

func createTLSConf() tls.Config {
	rootCertPool := x509.NewCertPool()
	pem, err := os.ReadFile("mysql/server-ca.pem")
	if err != nil {
		log.Fatal(err)
	}
	if ok := rootCertPool.AppendCertsFromPEM(pem); !ok {
		log.Fatal("Failed to append PEM.")
	}
	clientCert := make([]tls.Certificate, 0, 1)
	certs, err := tls.LoadX509KeyPair("mysql/client-cert.pem", "mysql/client-key.pem")
	if err != nil {
		log.Fatal(err)
	}
	clientCert = append(clientCert, certs)
	return tls.Config{
		RootCAs:            rootCertPool,
		Certificates:       clientCert,
		InsecureSkipVerify: true, // needed for self signed certs
	}
}

//https://stackoverflow.com/questions/67109556/connect-to-mysql-mariadb-with-ssl-and-certs-in-go
