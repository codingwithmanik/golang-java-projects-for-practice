package main

type A struct {
	string
	float64
}

func main() {
	a := A{}
	a.string = "Manik"
	a.float64 = 234
	//Func(4, 3) //Func(a, _ int)
	print("sohel\n")
}

/*
Introduction:
	This is reference manual for go.
Notation: Notations on Egle view of go code
	* func a(a ...int)
	* a(43, 32, 53);
    * arr := []int{3, 35, 6}; a(arr...);
Source Code Representation: done
	* characters
	* letters and digits
Lexical Elements: done
	* Comments
	* Tokens
	* Semicolons
	* Identifiers
	* Keywords
	* Operators and punctuation
	* Integer literals
	* Floating-point literals
	* Imaginary literals
	* Rune literals
	* String literals
Constants: done
Variables: done
Types: done
	* Numeric types
	* String types
	* Array types
	* Array types
	* Slice types
	* Struct types
	* Pointer types
	* Function types
	* Interface types
		* Basic interfaces
		* Embedded interfaces
		* General interfaces
		* Implementing an interface
	* Map types
	* Channel types
Properties of types and values: done
	* Underlying types
	* Core types
	* Type identify
	* Assignability
	* Representability
	* Method sets
Blocks: done
Declarations and scope:
	* Label scopes
	* Blank identifier
	* Predeclared identifiers
	* Exported identifiers
	* uniqueness of identifiers
	* Constant declarations
	* Iota
	* Type declarations
		* Alias declarations
		* Type definitions
		* Type parameter declarations
		* Type constraints
		* Variable declarations
		* Short variable declarations
		* Function declarations
		* Method declarations
Expressions:
	* Operands
	* Qualified identifiers
	* Composite literals
	* Function literals
	* Primary expressions
	* Selectors
	* Method expressions
	* Method values
	* Index expressions
	* Slice expressions
		* Simple slice expressions
		* Full slice expressions
	* Type assertions
	* Calls
	* Passing arguments to ... parameters
	* Instantiations
	* Type inference
		* Type unification
		* Function argument type inference
		* Constraint type inference
	* Operators
		* Operator precedence
	* Arithmetic operators
		* Integer operators
		* Integer overflow
		* Floating-point operators
		* String concatenations
	* Comparison operators
	* Logical operators
	* Address operators
	* Receive operators
	* Conversions
		* Conversions between numeric types
		* Conversion to and from a string type
		* Conversions from slice to array pointer
	* Constant expressions
	* Order of evaluation
Statements:
	* Terminating statements
	* Empty statements
	* Labeled statements
	* Expression statements
	* Send statements
	* IncDec statements
	* Assignments
	* if statements
	* Switch statements
		* Expression switches
		* Type switches
	* For statements
		* For statements with single condition
 		* For statements with for clause
		* For statements with range clause
	* Go statements
	* Select statements
	* Return statements
	* Break statements
	* Continue statements
	* Goto statements
	* Fallthrough statements
	* Defer statements
Built-in functions
	* Close
	* Length and capacity
	* Allocation
	* Making slices, maps and channels
	* Appending to and copying slices
	* Deletion of map elements
	* Manipulating complex numbers
	* Handling panics
	* Bootstrapping
Packages
	* Source file organization
	* Package clause
	* Import declarations
	* An example package
Program initialization and execution
	* The zero value
	* Package initialization
	* program execution
Errors:
Run-time panics:
System considerations:
	* Package unsafe
	* Size and alignment guarantees
*/
