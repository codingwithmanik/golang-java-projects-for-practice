package main

import "fmt"

type work string

func (w work) worker() {
	fmt.Println(w + " is working")
}

func doWork[T any](o T) {
	//o.worker() //type 'any' has not implemented worker
}
func main() {
	t := work("Manik")
	doWork(t) //doesn't work

	//make above workable using interface
	doWork2(t)

	//another thing: whatever we did in second case, it could be done by interface only not using generics
	doWork3(t)
	//now you will ask what is use of generics then? please see file generic.go
	//final outcome: generics is still in development mode lot more to come
}

//make above things workable using interface
type I interface {
	worker2()
}

func (w work) worker2() {
	fmt.Println(w + " is working")
}

func doWork2[T I](o T) {
	o.worker2()
}

func doWork3(o I) {
	o.worker2()
}
