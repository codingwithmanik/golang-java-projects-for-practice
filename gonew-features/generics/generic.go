package main

import "fmt"

func sumFloatOrInt[K comparable, V int64 | float64](m map[K]V) V {
	var s V
	for _, v := range m {
		s += v
	}
	return s
}

type Number interface {
	int64 | float64
}

func sumFloatOrInt2[K comparable, V Number](m map[K]V) V {
	var s V
	for _, v := range m {
		s += v
	}
	return s
}

func main() {
	fmap := map[string]float64{
		"first":  4.2,
		"second": 5.3,
	}
	imap := map[string]int64{
		"first":  5,
		"second": 6,
	}
	fmt.Println(sumFloatOrInt(fmap), "      ", sumFloatOrInt(imap))
	fmt.Println(sumFloatOrInt[string, float64](fmap), "      ", sumFloatOrInt[string, int64](imap))

	fmt.Println(sumFloatOrInt2(fmap), "      ", sumFloatOrInt2(imap))

	Print(5)
	Print("manik")

	//use as variables
	g := GenericSlice[int]{1, 2, 3}
	g.Print() //1 2 3
	Print2(g) //1 2 3
}

func Print[T any](a T) {
	fmt.Println(a)
}

//use as variables
type GenericSlice[T any] []T

func (g GenericSlice[T]) Print() {
	fmt.Println()
	for _, v := range g {
		fmt.Print(v)
	}
}

func Print2[T any](g GenericSlice[T]) {
	fmt.Println()
	for _, v := range g {
		fmt.Print(v)
	}
	fmt.Println()
}
