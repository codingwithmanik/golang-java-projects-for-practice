package main

import "fmt"

func sumFloat64(m map[string]float64) float64 {
	s := 0.0
	for _, v := range m {
		s += v
	}
	return s
}
func sumInt64(m map[string]int64) int64 {
	s := int64(0)
	for _, v := range m {
		s += v
	}
	return s
}

func main() {
	fmap := map[string]float64{
		"first":  4.2,
		"second": 5.3,
	}
	imap := map[string]int64{
		"first":  5,
		"second": 6,
	}
	fmt.Printf("value of sfloat64=%g and sint64=%d\n", sumFloat64(fmap), sumInt64(imap))
}
