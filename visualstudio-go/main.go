package main

import (
	"fmt"
	"gowithvcode/pkg"

	"rsc.io/quote"
)

func main() {
	fmt.Print(pkg.ReturnMessage())
	fmt.Println(quote.Go())
}
