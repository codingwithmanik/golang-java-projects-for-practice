package tests

import (
	"gowithvcode/pkg"
	"testing"
)

func TestMessage(t *testing.T) {
	if pkg.ReturnMessage() != "Hello World" {
		t.Fatal("Wrong Message")
	}
}
