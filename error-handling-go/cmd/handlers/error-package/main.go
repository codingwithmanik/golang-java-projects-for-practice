package main

import (
	"errors"
	"fmt"
)

func main() {
	//Example of errors.Unwrap()
	err1 := errors.New("leaf error")
	err2 := fmt.Errorf("intermediate: %w", err1)
	err3 := fmt.Errorf("root: %w", err2)
	//If errors.Unwrap(e) returns a non-nil error w, then we say that e wraps w.
	err4 := errors.Unwrap(err3)
	fmt.Println(err4) //intermediate: leaf error
	err5 := errors.Unwrap(err4)
	fmt.Println(err5) //leaf error

	//Example of errors.As()
	var be *badError
	er1 := fmt.Errorf("error msg: %w", &badError{"bad input"})
	if errors.As(er1, &be) {
		fmt.Println("got error")
	}
}

type badError struct {
	errMsg string
}

func (e *badError) Error() string {
	return fmt.Sprintf("bad input: %s", e.errMsg)
}
