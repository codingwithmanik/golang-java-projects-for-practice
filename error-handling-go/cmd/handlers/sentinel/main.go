package main

import (
	"errors"
	"handle-error/pkg/error.types/sentinel"
)

// It is worth mentioning that many times all we care about is checking that an error is not nil. However, there are other situations that it will make sense to know the specific error,
//maybe to handle different error scenarios. For these situations, it is important how we create those errors in order to be handled easily.

func main() {
	err := sentinel.PushData(1)
	// As you probably noticed this works and fulfils our purpose, so we could think we are fine. However, this type of error checking is quite fragile since if the maintainers of the
	// sentinel package decides to update the string value of the CustomError, the program would break in all the places where this error is being checked like this.
	if err.Error() == sentinel.ErrBadInput.Error() {
		//your codes
	}
	//So, inspecting errors output should never be used for error handling.

	//As you will see in the last section, this kind of error handling even makes more sense thanks to the error wrapping capability.
	//The main benefit of this approach over inspecting the error.Error() output is that here we are comparing the error we got against the CustomError value. Not only is it cleaner, but our
	//code automatically becomes more robust as any change to the CustomError string value will not end up in a breaking change in our code.
	if errors.Is(err, sentinel.ErrNotFound) {
		//your codes
	}

	//It will not be possible to compare the error value with none of the previously mentioned strategies, since the error is customized but not static
	_, err = sentinel.FindData(1)
}
