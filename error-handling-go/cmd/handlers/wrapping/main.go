package main

import (
	"errors"
	"fmt"
	"handle-error/pkg/error.types/wrapping"
)

//When Go 1.13 was released, the maintainers decided to extend the fmt.Errorf method to support a new verb %w which basically performs error wrapping under the hood
//in a similar way like the Wrap method of the no longer maintained github.com/pkg/errors package.

func main() {
	_, err := wrapping.FindData(1)

	if errors.Is(err, wrapping.ErrNotFound) {
		fmt.Println("Data not found(404)")
	}
}
