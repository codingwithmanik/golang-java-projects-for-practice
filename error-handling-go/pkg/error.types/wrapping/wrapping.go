package wrapping

import (
	"errors"
	"fmt"
)

var (
	ErrBadInput  = errors.New("bad input")
	ErrNotFound  = errors.New("not found")
	ErrForbidden = errors.New("forbidden")
	data         = make(map[int]int)
)

func FindData(v int) (int, error) {
	d, ok := data[v]
	if !ok {
		return 0, fmt.Errorf("data: %v, error: %w", v, ErrNotFound)
	}
	return d, nil
}
