// Package sentinel: A sentinel error is a named error value. Normally these errors are exported identifiers since the idea is that
// users using your API can compare the errors against this given value
package sentinel

import (
	"errors"
	"fmt"
)

var (
	ErrBadInput  = errors.New("bad input")
	ErrNotFound  = errors.New("not found")
	ErrForbidden = errors.New("forbidden")
	data         = make(map[int]int)
)

func PushData(v int) error {
	return createError(v)
}

func FindData(v int) (int, error) {
	d, ok := data[v]
	if !ok {
		return 0, fmt.Errorf("data %v, error: %v", v, ErrNotFound)
	}
	return d, nil
}

func createError(v int) error {
	switch v {
	case 0:
		return ErrBadInput
	case 1:
		return ErrNotFound
	case 2:
		return ErrForbidden
	}
	return nil
}
