## TOC
[Error Package](cmd/handlers/error-package/main.go)

[Error Types]()<br>
&nbsp; [Sentinel](cmd/handlers/sentinel/main.go)<br>
&nbsp; [Custom](cmd/handlers/custom/main.go)<br>
&nbsp; [Wrapping](cmd/handlers/wrapping/main.go)

## Summarize about which one to practice when
To summarize the key takeaways, we could state that there are different things to take into account when handling errors in Go:

First, if we do not care about what is the error, just check its nullness; end of the story. Still, there are many times when we will need to know what the error is in order to handle different error scenarios. When this matters, we should mainly consider two options:

```&nbsp; Sentinel errors when we do not need dynamic values. We can compare them using errors.Is.```

```&nbsp; Wrapped errors when we need to combine a sentinel error with dynamic information.```

I hope you enjoyed the article and learned something new about how to handle errors in Go.