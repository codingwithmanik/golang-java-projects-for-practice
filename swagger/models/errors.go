package models

type ServerError struct {
	Messages []string `json:"messages"`
}