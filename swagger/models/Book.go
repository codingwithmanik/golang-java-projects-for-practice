package models

import "github.com/jinzhu/gorm"

// swagger:model
type Book struct {
	ID uint  `gorm:"primaryKey"`
	BookName string
	BookAuthor string
	PublishedDate string
	Price float32
	Quantity int
}

func DBMigrate(db *gorm.DB) *gorm.DB {
	db.AutoMigrate(&Book{})
	return db
}

// swagger:model
type Author struct {
	ID uint  `gorm:"primaryKey"`
	FirstName string
	LastName string
	BookId int
	Address string
}