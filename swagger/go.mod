module swagger-demo

go 1.14

require (
	github.com/asaskevich/govalidator v0.0.0-20210307081110-f21760c49a8d // indirect
	github.com/go-chi/chi v3.3.2+incompatible
	github.com/go-openapi/errors v0.20.0 // indirect
	github.com/go-openapi/validate v0.20.2 // indirect
	github.com/go-sql-driver/mysql v1.5.0
	github.com/go-swagger/go-swagger v0.26.1 // indirect
	github.com/jinzhu/gorm v1.9.16
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/spf13/afero v1.5.1 // indirect
	go.mongodb.org/mongo-driver v1.5.0 // indirect
	golang.org/x/mod v0.4.1 // indirect
	golang.org/x/net v0.0.0-20210226172049-e18ecbb05110 // indirect
	golang.org/x/sys v0.0.0-20210309074719-68d13333faf2 // indirect
	golang.org/x/tools v0.1.0 // indirect
)
