# go-mysql-crud
Sample crud operation using Golang and MySql

## API ENDPOINTS

### All Posts
- Path : `/posts`
- Method: `GET`
- Response: `200`

### Create Post
- Path : `/posts`
- Method: `POST`
- Fields: `title, content`
- Response: `201`

### Details a Post
- Path : `/posts/{id}`
- Method: `GET`
- Response: `200`

### Update Post
- Path : `/posts/{id}`
- Method: `PUT`
- Fields: `title, content`
- Response: `200`

### Delete Post
- Path : `/posts/{id}`
- Method: `DELETE`
- Response: `204`

### Setup Swagger
brew tap go-swagger/go-swagger
brew install go-swagger

### Convert doc from v2 to v3
- [link](https://levelup.gitconnected.com/go-swagger-and-open-api-e6b6ea4ce48f#:~:text=of%20our%20package.-,Converting,-Second%20step%20is)

## Required Packages
- Dependency management
    * [dep](https://github.com/golang/dep)
- Database
    * [MySql](https://github.com/go-sql-driver/mysql)
- Routing
    * [chi](https://github.com/go-chi/chi)

## Quick Run Project
First clone the repo then go to go-mysql-crud folder. After that build your image and run by docker. Make sure you have docker in your machine. 

```
git clone https://github.com/s1s1ty/go-mysql-crud.git

cd go-mysql-crud

chmod +x run.sh
./run.sh

docker-compose up
```

## Resources 
#### There are two popular tools go-swagger and swag
- Go-Swagger [repo](https://github.com/go-swagger/go-swagger) & [documentation](https://goswagger.io/generate/spec.html)
- Swaggo [repo](https://github.com/swaggo/swag)  & [tutorial](https://medium.com/wesionary-team/automatically-generate-restful-api-documentation-in-golang-76927f8f8935)
