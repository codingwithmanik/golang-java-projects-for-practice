package impl

import (
	"net/http"
)

// swagger:route DELETE /delete-author/id author DeleteAuthorById
// Delete an Author
//
// Description: For deleting an author you have to provide id attached.
//
// Responses:
//	200: bookResponse
//  500: serverErrorResponse
func DeleteAuthorById(writer http.ResponseWriter, request *http.Request) {
}

// swagger:route GET /get-author-byid/id author GetAuthorById
// Get an author
//
// Description: Retrieve an author by providing an id.
//
// Responses:
//	201: bookResponse
//  500: serverErrorResponse
func GetAuthorById(writer http.ResponseWriter, request *http.Request) {
}

// swagger:route PUT /update-author/id author UpdateAuthor
// Update by id
//
// Description: Update a individual author by providing id.
// responses:
//	200: bookResponse
//  500: serverErrorResponse
func UpdateAuthor(writer http.ResponseWriter, request *http.Request) {
}

// swagger:route POST /create-author author CreateAuthor
// Create Author
//
// Description: Please provide request data to be created
// responses:
//	201: bookResponse
//  500: serverErrorResponse

// Create handles POST requests to add new books
func CreateAuthor(writer http.ResponseWriter, request *http.Request) {

}
