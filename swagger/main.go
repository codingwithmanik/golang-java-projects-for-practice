package main
import (
	"encoding/json"
	"fmt"
	"github.com/go-chi/chi"
	"github.com/jinzhu/gorm"
	_ "github.com/go-sql-driver/mysql"
	"log"
	"net/http"
	"strconv"
	"swagger-demo/models"
)
var DB *gorm.DB
func main() {
	fmt.Println("Server listen at :8005")
	http.ListenAndServe(":8005",BookRouter())
}


// A completely separate router for posts routes
func  BookRouter() http.Handler {
	DB =Instantiate().DB;
	r := chi.NewRouter()
	r.Post("/create-book", CreateBook)
	r.Put("/update-book/{id:[0-9]+}",UpdateBook)
	r.Get("/get-all-book",GetAllBook)
	r.Get("/get-book-by-id/{id:[0-9]+}", GetBookById)
	r.Delete("/delete-book-by-id/{id:[0-9]+}",DeleteBookById)
	return r
}
// swagger:route DELETE /delete-book/id books DeleteBookById
// Delete a book
//
// Description: For deleting a book you have to provide id attached.
//
// Responses:
//	20o: bookResponse
//  500: serverErrorResponse
func DeleteBookById(writer http.ResponseWriter, request *http.Request) {
	var bk models.Book;
	id:=chi.URLParam(request,"id");
	DB.Unscoped().Where("ID=?",id).Delete(&bk)//.Unscoped();
	writer.Header().Set("Content-Type", "application/json")
	writer.Write([]byte("Successfully deleted"));
}
// swagger:route GET /get-book-byid/id books GetBookById
// Get a book
//
// Description: Retrieve a book by providing an id.
//
// Responses:
//	201: bookResponse
//  500: serverErrorResponse
func GetBookById(writer http.ResponseWriter, request *http.Request) {
	var bk models.Book;
	id:=chi.URLParam(request,"id");
	DB.Where("ID=?",id).Find(&bk)
	newsJson, _ := json.Marshal(bk)
	writer.Header().Set("Content-Type", "application/json")
	writer.Write(newsJson)
}
// swagger:route GET /get-all-books books GetAllBook
// Get all books
//
// Description: Retrieve all the books from the db.
//
// Responses:
//	201: bookResponse
//  500: serverErrorResponse
func GetAllBook(writer http.ResponseWriter, request *http.Request) {
	var bk[] models.Book;
	DB.Find(&bk);
	newsJson, err := json.Marshal(bk)
	if err != nil {
		panic(err)
	}
	writer.Header().Set("Content-Type", "application/json")
	writer.Write(newsJson)
}

// swagger:route PUT /update-book/id books UpdateBook
// Update by id
//
// Description: Update a individual book by providing id.
// responses:
//	200: bookResponse
//  500: serverErrorResponse
func UpdateBook(writer http.ResponseWriter, request *http.Request) {
	var bk models.Book; var temp models.Book;
	err:=json.NewDecoder(request.Body).Decode(&temp)
	id:=chi.URLParam(request,"id");
	DB.Find(&bk).Where("ID=?",id)
	bk.BookName=temp.BookName; bk.BookAuthor=temp.BookAuthor; bk.Price=temp.Price; bk.Quantity=temp.Quantity;
	bk.PublishedDate=temp.PublishedDate;
	if err!=nil{
		panic(err)
	}
	DB.Save(&bk);
	writer.Write([]byte("Successfully updated"));
}
// swagger:route POST /create-book books CreateBook
// Create book
//
// Description: Please provide request data to be created
// responses:
//	201: bookResponse
//  500: serverErrorResponse

// Create handles POST requests to add new books
func CreateBook(writer http.ResponseWriter, request *http.Request) {
	var bk models.Book;
	// fetch the book from the request
	err:=json.NewDecoder(request.Body).Decode(&bk)
	if err!=nil{
		panic(err)
	}
	DB.Create(&bk);
	writer.Write([]byte("Successfully created with id_"+strconv.Itoa(int(bk.ID))));
}

type DataProvider struct {
	DB *gorm.DB
}

func  Instantiate() *DataProvider{
	dbURI := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?parseTime=true", "root", "", "localhost", 3306, "swagger");
	gormConn, err := gorm.Open("mysql", dbURI)
	if err != nil {
		log.Fatal("Could not connect database")
	}
	models.DBMigrate(gormConn)
	return &DataProvider{gormConn}
}

