// Package classification Book Microservice
//
// Documentation for book microservice
//
// TOS:
//  There are no TOS at this moment, use at your own risk we take no responsibility
//
//	Schemes: http, https
//  Host: api.synergyforce.ca
//	BasePath: /bookservices/
//	Version: 1.0.0
//  License: SF http://synergyforce.ca
//  Contact: Manik Hossain<manik37343902@gmail.com> http://manik.hossain.com
//
//	Consumes:
//	- application/json
//
//	Produces:
//	- application/json
//
// swagger:meta
package main

import "swagger-demo/models"

// Data structure representing a single product
// swagger:response bookResponse
type bookResponseWrapper struct {
	// Newly created book
	// in: body
	Body models.Book
}

// Data structure representing a single product
// swagger:response serverErrorResponse
type serverErrorWrapper struct {
	// in: body
	Body models.ServerError
}
