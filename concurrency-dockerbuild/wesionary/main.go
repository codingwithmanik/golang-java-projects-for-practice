package main

import (
	"fmt"
	"net/http"
	"sync"
	"time"
)

var websites = []string{
	"https://stackoverflow.com/",
	"https://github.com/",
	"https://www.linkedin.com/",
	"http://medium.com/",
	"https://golang.org/",
	"https://www.udemy.com/",
	"https://www.coursera.org/",
	"https://wesionary.team/",
}

var wg sync.WaitGroup
var mut sync.Mutex

//Using WaitGroup
func main1() {
	start := time.Now()
	for _, website := range websites {
		wg.Add(1)
		go getWebsite1(website)
	}
	wg.Wait()
	fmt.Println("Total time: ", time.Since(start))
}

func getWebsite1(website string) {
	defer wg.Done()
	if res, err := http.Get(website); err != nil {
		fmt.Println(website, "is down")

	} else {
		fmt.Printf("[%d] %s is up\n", res.StatusCode, website)
	}
}

/*
Mutex(Mutually Exclusive Lock) is another synchronization mechanism. It synchronizes access to shared resources. So if there is a
situation when a resource could be used by multiple Goroutines simultaneously we can make use of a mutex. Once a Goroutine request
a Mutex lock on a shared resource using sync.Mutex.Lock() , other Goroutines cannot access that shared resource until the mutex is
unlocked using sync.Mutex.Unlock() .
*/
//Using Mutex
func main2() {
	start := time.Now()
	for _, website := range websites {
		go getWebsite2(website)
		wg.Add(1)
	}
	wg.Wait()
	fmt.Println("Total time: ", time.Since(start))
}
func getWebsite2(website string) {
	defer wg.Done()
	if res, err := http.Get(website); err != nil {
		fmt.Println(website, "is down")

	} else {
		mut.Lock()
		defer mut.Unlock()
		fmt.Printf("[%d] %s is up\n", res.StatusCode, website)
	}

}

//Using Channel but facing problem with closing channel when iterating over channel
func main3() {
	start := time.Now()
	c := make(chan string)
	for _, website := range websites {
		go getWebsite3(website, c)
	}

	//Iterating over the range of channel. So keeps receiving messages until channel is closed
	for msg := range c {
		fmt.Println(msg)
	}
	//Alternate syntax
	//for {
	//	msg, open := <-c
	//	if !open {
	//		break
	//	}
	//	fmt.Println(msg)
	//}
	fmt.Println("Total time: ", time.Since(start))
}
func getWebsite3(website string, c chan string) {
	if _, err := http.Get(website); err != nil {
		c <- website + "is down"

	} else {
		c <- website + " is up"
	}
}

func main4() {
	start := time.Now()
	c := make(chan string)
	for _, link := range websites {
		wg.Add(1)
		go getWebsite4(link, c)
	}
	go func() {
		wg.Wait()
		close(c)
	}()
	for msg := range c {
		fmt.Println(msg)
	}
	fmt.Println(time.Since(start))
}
func getWebsite4(url string, c chan string) {
	defer wg.Done()
	_, err := http.Get(url)
	if err != nil {
		c <- url + "is down"
	} else {
		c <- url + " is up"
	}
}
