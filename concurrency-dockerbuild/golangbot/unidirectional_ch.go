package main

import "fmt"

// All the channels we discussed so far are bidirectional channels, that is data can be both sent and received on them. It is also possible to
// create unidirectional channels, that is channels that only send or receive data.
func sendData(sendch chan<- int) {
	sendch <- 10
}

func main1() {
	sendch := make(chan<- int)
	go sendData(sendch)
	//fmt.Println(<-sendch) //Error
}

//Got compile time error.types at line 12
//All is well but what is the point of writing to a send only channel if it cannot be read from!

//This is where channel conversion comes into use. It is possible to convert a bidirectional channel to a send only or receive only channel but
//not the vice versa.

func main2() {
	chnl := make(chan int)
	go sendData(chnl)
	fmt.Println(<-chnl)
}

/*In line no. 24 of the program above, a bidirectional channel chnl is created. It is passed as a parameter to the sendData Goroutine in line
no. 25. The sendData function converts this channel to a send only channel in line no. 7 in the parameter sendch chan<- int. So now the channel
is send only inside the sendData Goroutine but it's bidirectional in the main Goroutine. This program will print 10 as the output.
*/
