package main

/*Sends and receives are blocking by default(Unbuffered Channels)
Sends and receives to a channel are blocking by default. What does this mean? When data is sent to a channel, the control is blocked in the
send statement until some other Goroutine reads from that channel. Similarly, when data is read from a channel, the read is blocked until
some Goroutine writes data to that channel.

This property of channels is what helps Goroutines communicate effectively without the use of explicit locks or conditional variables that
are quite common in other programming languages.

It's ok if this doesn't make sense now. The upcoming sections(Buffered Channels) will add more clarity on how channels are blocking by default.
*/
