package main

import . "fmt"

/*What to learn
1. link: https://golangbot.com/mutex/
2. What is mutex
3. Why it's needed
4. What is race condition
5. How to solve RC using Mutex
6. How to solve RC using Channel
7. Mutex vs Channel
*/

type X interface {
	m1()
}
type Y interface {
	X
	m2()
}
type handler struct{}

func (handler) m1() {
	Println("From m1")
}
func (handler) m2() {
	Println("From m2")
}

type A struct {
	FirstName string
}
type B struct {
	A
	LastName string
}

//practice code
func main() {
	var all B
	all.FirstName = "Manik"
	all.LastName = "Hossain"
	Println("Full name: " + all.FirstName + " " + all.LastName)

	var i Y
	i = &handler{}
	i.m1()
	i.m2()
}
