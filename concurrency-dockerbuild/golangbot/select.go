package main

import (
	"fmt"
	"time"
)

/* Topics
1.link: https://golangbot.com/select/
2.When to use, practical uses, default case.
3.Deadlock
4.Random selection
5.Gotcha - Empty select
*/

func server1(ch chan string) {
	time.Sleep(6 * time.Second)
	ch <- "from server1"
}
func server2(ch chan string) {
	time.Sleep(3 * time.Second)
	ch <- "from server2"

}

//use to give user fast response for the same expectation from different servers; It will print "from server2" and return
func main() {
	output1 := make(chan string)
	output2 := make(chan string)
	go server1(output1)
	go server2(output2)
	select {
	case s1 := <-output1:
		fmt.Println(s1)
	case s2 := <-output2:
		fmt.Println(s2)
	}
}

/*
func main() {
    select {}
}
We know that the select statement will block until one of its cases is executed. In this case, the select statement doesn't have any cases
and hence it will block forever resulting in a deadlock.
*/
