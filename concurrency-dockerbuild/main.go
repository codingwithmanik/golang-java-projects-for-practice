package main

import (
	"fmt"
	"github.com/urfave/cli/v2"
	"log"
	"os"
	"time"
)
/* We use more than one thread to run our task asynchronously. Concurrency is not the same with running tasks
   at the same time.
*/
func a(done chan string){
	var i int=1;
	for i<=5{
		time.Sleep(time.Millisecond*250)
		fmt.Println("a: ",i)
		i++
	}
	done<-"done"
}
//              1  2  3  4  5 //sleep 1 second
//      start:  1  2  3  4  5  6  7  8  9  10
//                 1     2     3     4     5 //sleep 2 second

//                a     b     c      d     e  //sleep(250)
//      start:   250   500   750   1000   1250   1500   1750   2000   2250   2500
//                    1         2        3           4           4 //sleep(400)
func b(){
	var i int=1;
	for i<=5{
		time.Sleep(time.Millisecond*400)
		fmt.Println("b: ",i)
		i++
	}
}
func main(){
	var ch chan string
	ch = make(chan string)  //in one line ch:=make(chan int)
	go a(ch)
	go b()
    result:=<-ch
    fmt.Println(result)
}







func main3(){
	app := &cli.App{
		Flags: []cli.Flag {
			&cli.StringFlag{
				Name: "lang",
				Aliases: []string{"l"},
				Value: "english",
				Usage: "language for the greeting",
			},
			&cli.StringFlag{
				Name:    "start-infrastructure",
				Value:   "",
				Aliases: []string{"si"},
				Usage:   "-si true  [deploys redis, postgres, etcd, sqs, elasticsearch, kinesis]",
			},
		},
		Action: func(c *cli.Context) error {
			name := "Nefertiti"
            fmt.Println(c.String("start-infrastructure")) // english

			if c.String("lang") == "spanish" {
				fmt.Println("Hola", name)
			} else {
				fmt.Println("Hello", name)
			}
			return nil
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}