## Application on dirsize
Youtube [link](https://www.youtube.com/watch?v=r7qm6ZrxYIE) <br>
Github [link](https://github.com/jarifibrahim/workshop) <br>
Topics:
  - `Goroutine` and `Channels`
  - `Buffered` and `Unbuffered(Synchronous)` channels
  - `Buffered` -> channels with capacity
  - `Unbuffered` -> also called synchronous channel which has no capacity

## Other sources
- From [golangprograms.com](https://www.golangprograms.com/go-language/concurrency.html)
- From [gobyexample.com](https://gobyexample.com/stateful-goroutines)
- From [oreilly.com](https://www.oreilly.com/library/view/concurrency-in-go/9781491941294/ch04.html)
- From [medium.com](https://medium.com/compass-true-north/concurrent-programming-in-go-de33441ace1c)
- From [thepolyglotdeveloper.com](https://www.thepolyglotdeveloper.com/2017/05/concurrent-golang-applications-goroutines-channels/)
- From [astaxie.gitbooks.io](https://astaxie.gitbooks.io/build-web-application-with-golang/content/en/02.7.html)