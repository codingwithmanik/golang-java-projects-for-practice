package main

import "fmt"

// https://stackoverflow.com/questions/27775376/value-receiver-vs-pointer-receiver
// syntax 1
type myString string

func (m myString) myFunc() {
	m = "Changed"
}
func (m *myString) myFunc2() {
	*m = "Changed"
}
func main() {
	b := myString("Kamal")
	b.myFunc()
	fmt.Println(b) //Kamal
	c := myString("Hossain")
	c.myFunc2()
	fmt.Println(c) //Changed
}

/* //syntax 2
type Vertex struct{
	a in
}
func Change1(v *Vertex){
	v.a= 7 //change value of a
}
func (v *Vertex) Change2(){
	v.a = 7; //change value of a
}
func (v Vertex) Change3(){
    v.a = 7; //change value of a
}
func main2(){
	v := Vertex{
		1,
	}
	Change1(v)  // Compile error.types!
	Change1(&v) // OK

	v.Change2()    //OK
	(&v).Change2() //OK
	//For the statement v.Change2(), even though v is a value and not a pointer, the method with the pointer receiver
	//is called automatically. That is, as a convenience, Go interprets the statement v.Change2() as (&v).Change2()
	//since the Change() method has a pointer receiver.

    v.Change3() //OK
    p:= &v
    p.Change3() //OK
    //In this case, the method call p.Change3() is interpreted as (*p).Change3()
} */
/*Choosing a value or pointer receiver
1.There are two reasons to use a pointer receiver.
2.The first is so that the method can modify the value that its receiver points to.
3.The second is to avoid copying the value on each method call. This can be more efficient if the receiver is a
  large struct, for example.
4.In this example, both Scale and Abs are with receiver type *Vertex, even though the Abs method needn't modify its receiver.
5.In general, all methods on a given type should have either value or pointer receivers, but not a mixture of both. */
