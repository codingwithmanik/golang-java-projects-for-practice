package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	var t int8
	fmt.Scan(&t)
	r := bufio.NewReader(os.Stdin)
	for i := int8(0); i < t; i++ {
		ch, _ := r.ReadByte()
		r.ReadByte()
		if strings.Contains("codeforces", string(ch)) {
			fmt.Println("YES")
		} else {
			fmt.Println("NO")
		}
	}
}
