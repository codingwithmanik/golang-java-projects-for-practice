package child

type Child interface {
	GetChild(name string) string
	GetAge() string
}

type handler struct {
}

func NewChild() Child {
	handler := &handler{}
	return handler
}

func (i handler) GetChild(name string) string {
	return "Child name: " + name
}

func (i handler) GetAge() string {
	return "\nAge= 5"
}
