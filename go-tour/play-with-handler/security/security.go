package security

type Security interface {
	ValidateToken(token string) bool
}
type handler struct {
}

func (h handler) ValidateToken(token string) bool {
	return true
}

func NewSecurity() Security {
	h := &handler{}
	return h
}
