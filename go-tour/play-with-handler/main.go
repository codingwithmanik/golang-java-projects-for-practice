package main

import (
	"aaa/child"
	"aaa/dashboard"
	"aaa/security"
	"fmt"
)

type handler struct {
	childSvc     child.Child
	dashboardSvc dashboard.Dashboard
	security     security.Security
}

func NewAppProvider() *handler {
	a := &handler{}
	a.childSvc = child.NewChild()
	a.dashboardSvc = dashboard.NewDashboard()
	a.security = security.NewSecurity()
	return a
}

func main() {
	handler := NewAppProvider()

	fmt.Println(handler.childSvc.GetChild("Manik"))
}
