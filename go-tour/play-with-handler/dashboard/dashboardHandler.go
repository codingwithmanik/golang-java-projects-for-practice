package dashboard

type Dashboard interface {
	UserDashboard() string
}
type handler struct {
}

func (h handler) UserDashboard() string {
	return "Printing dashboard data"
}

func NewDashboard() Dashboard {
	h := &handler{}
	return h
}
