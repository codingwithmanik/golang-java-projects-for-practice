package main

import (
	"errors"
	"fmt"
	"log"
)

func main() {
	err := m1()
	err2 := m2()
	if err != nil {
		fmt.Println(errors.Is(err, err2))
		fmt.Println(errors.Unwrap(err2))
	}
	log.Print(err)
}

func m1() error {
	return errors.New("null pointer exception")
}
func m2() error {
	return errors.New("index out of bound exception")
}
