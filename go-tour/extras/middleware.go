package main
//https://stackoverflow.com/questions/68677347/how-does-middleware-work-in-chi-routing-in-go-and-what-does-http-handler-argumen
//https://www.geeksforgeeks.org/anonymous-function-in-go-language/
import "fmt"

type handler func()

// your handler
func f() { fmt.Println("f") }

// one middleware
func g(next handler) handler {
	return func() {
		fmt.Print("g.")
		next()
	}
}

// another middleware
func h(next handler) handler {
	return func() {
		fmt.Print("h.")
		next()
	}
}


func main() {
	h1 := h(g(f))
	h1()

	h2 := g(h(f))
	h2()

	// And you can chain as many of these as you like
	// and in any order you like.
	h3 := h(g(h(h(h(g(g(h(f))))))))
	h3()
}
