package main

import (
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
)

func main() {
	rsp := GetHostIP()
	pIP := GetPublicIP()
	fmt.Println("Host ip " + rsp)
	fmt.Println("public ip " + pIP)
}

func GetHostIP() string {
	netInterfaceAddresses, err := net.InterfaceAddrs()

	if err != nil {
		return ""
	}

	for _, netInterfaceAddress := range netInterfaceAddresses {
		networkIp, ok := netInterfaceAddress.(*net.IPNet)
		if ok && !networkIp.IP.IsLoopback() && networkIp.IP.To4() != nil {
			ip := networkIp.IP.String()
			//fmt.Println("Resolved Host IP: " + ip)
			return ip
		}
	}
	return ""
}

func GetPublicIP() string {
	url := "https://api.ipify.org?format=text"
	// we are using a pulib IP API, we're using ipify here, below are some others
	// https://www.ipify.org
	// http://myexternalip.com
	// http://api.ident.me
	// http://whatismyipaddress.com/api
	//fmt.Printf("Getting IP address from  ipify ...\n")
	resp, err := http.Get(url)
	if err != nil {
		panic(err)
	}
	defer func() { _ = resp.Body.Close() }()
	ip, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	return fmt.Sprintf("%s", ip)
}
