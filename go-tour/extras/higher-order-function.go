package main

import "fmt"

/* Two types
takes one or more functions as arguments
returns a function as its result
*/

//takes one or more functions as arguments
func simple(a func(a, b int) bool) {
	fmt.Println(a(5, 7))
}

//returns a function as its result
func simple2() func(a, b int) int {
	return func(a, b int) int {
		return a + b
	}
}

func main() {
	f := func(a, b int) bool {
		return a < b
	}
	simple(func(a, b int) bool {
		return a > b
	})
	simple(f)

	var s func(a, b int) int
	s = simple2()
	fmt.Println(s(5, 6))
}
