package main

//func main() {
//	var a int = 23
//	var b, c = &a, &a
//	fmt.Println(&a, b, c) // 0x1040a124 0x1040a124
//	fmt.Println(&b, &c)   // 0x1040c108 0x1040c110
//	*b++
//	println(*b, " ", a, " ", *c)
//}

func main() {
	var n int
	n = 5
	var m *int
	m = &n
	println(&m, m, &n)
	println(n)
	fn(m)
	println(n)
}

func fn(m *int) {
	println(&m, m)
	*m++
	println(m)
}
