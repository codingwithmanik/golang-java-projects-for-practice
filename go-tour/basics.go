package main

import (
	"fmt"
)

/* //Named return values
//  1.A return statement without arguments returns the named return values. This is known as
//    a "naked" return.
//  2.Naked return statements should be used only in short functions, as with the example shown here.
//    They can harm readability in longer functions.
func namedReturns() (x,y int){
	//var x,y int     //note:This will treat as redeclared
	x = 5
	y = 6
	return
}
func main(){
	r1,r2 := namedReturns()
	fmt.Println(r1,r2)
} */

/*
//If an initializer is present, the type can be omitted; the variable will take the type of the initializer.
var i, j int = 1, 2
func main() {
	var c, python, java = true, false, "no!"
	fmt.Println(i, j, c, python, java)

    //for {   //for ever for loop
    //	fmt.Println("Manik ISlam")
	//}

	//fmt.Println("When's Saturday?") //switch case
	//today := time.Now().Weekday()
	//fmt.Println(today)
	//switch time.Saturday {
	//case today + 0:
	//	fmt.Println("Today.")
	//case today + 1:
	//	fmt.Println("Tomorrow.")
	//case today + 2:
	//	fmt.Println("In two days.")
	//default:
	//	fmt.Println("Too far away.")
	//}
} */


/* //Type conversations
var i int = 42
var f float64 = float64(i)
var u uint = uint(f)
//Or, put more simply:
i := 42
f := float64(i)
u := uint(f)  */

/* learn about defer, panic, recover
https://go.dev/blog/defer-panic-and-recover
*/

/*
type Emp struct{
	a int
}
func main(){
	a := 1e5
	fmt.Println(a) // a = 100000
	obj := Emp{5}
	p := &obj
	fmt.Printf("Type obj %T and type of p is %T\n",obj,p) //main.Emp  *main.Emp
	fmt.Println((*p).a) //To access the field X of a struct when we have the struct so the language
	fmt.Println(p.a)    //pointer p we could write (*p).X. However, that notation is cumbersome, permits us
					    // instead to write just p.X, without the explicit dereference.
} */

/* //map
func main() {
	m := make(map[string]int)
	m["Answer"] = 42
	fmt.Println("The value:", m["Answer"])
	delete(m, "Answer")
	v, ok := m["Answer"]
	fmt.Println("The value:",v, "    Present?", ok)
} */







/*
//Function values : Important
//Functions are values too. They can be passed around just like other values.
//Function values may be used as function arguments and return values.
func compute(fn func(float64, float64) float64) float64 {
	return fn(3, 4)
}
func main() {
	hypot := func(x, y float64) float64 {
		return math.Sqrt(x*x + y*y)
	}
	fmt.Println(hypot(5, 12))
	fmt.Println(compute(hypot))
	fmt.Println(compute(math.Pow)) //Pow(3^4) method signature: Pow(x, y float64) float64
} */


/*
//Func closures
//Go functions may be closures. A closure is a function value that references variables from outside its body.
//The function may access and assign to the referenced variables; in this sense the function is "bound" to the variables.
//For example, the adder function returns a closure. Each closure is bound to its own sum variable.
 */
func adder() func(int) int {
	sum := 0
	return func(x int) int {
		sum += x
		return sum
	}
}
func main() {
	pos, neg := adder(), adder()
	for i := 0; i < 5; i++ {
		fmt.Println(
			pos(i),
			neg(-2*i),
		)
	}
}