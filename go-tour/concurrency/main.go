package main

import (
	"fmt"
	"runtime/debug"
)

var v string

func version() string {
	if v == "" {
		bi, ok := debug.ReadBuildInfo()
		if ok {
			v = bi.Main.Version
		} else {
			v = "dev"
		}
	}
	return v
}
func main() {
	go func() {
		fmt.Println(version())
	}()
	fmt.Println(version())
}
