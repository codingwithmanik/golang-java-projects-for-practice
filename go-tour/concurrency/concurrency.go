package main

import (
	"sync"
)

var i int
var mutex sync.Mutex

//A read/write mutex allows all the readers to access the map at the same time, but a writer will lock out everyone else.
//var mutex sync.RWMutex

var wait sync.WaitGroup

//func say(s string) {
//	for i := 0; i < 2; i++ {
//		time.Sleep(100 * time.Millisecond)
//		fmt.Println(s)
//	}
//}
func main() {
	//important line: Goroutines run in the same address space, so access to shared memory must be synchronized
	//go say("world")
	//say("hello")

	for i := 0; i < 1000; i++ {
		wait.Add(1)
		go increment()
	}
	wait.Wait()
	println(i)
}
func increment() {
	mutex.Lock()
	i++
	mutex.Unlock()
	wait.Done()
}
