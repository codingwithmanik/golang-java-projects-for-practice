package main

import "sync"

/*The right way to implement a singleton pattern in Go is to use the sync package’s Once.Do() function.
This function makes sure that your specified code is executed only once and never more than once.*/

/* syntax :
once.Do(func() {
	// This will be executed
	// only once in the entire lifetime of the program
})
*/

var once sync.Once
var conn string

func dbConn() string {
	// is going to be executed only once in the entire lifetime
	once.Do(func() {
		conn = "Connected"
		println("Inside")
	})
	println("Outside")
	return conn
}
func main() {
	for i := 0; i < 5; i++ {
		dbConn()
	}
}
