package main

import "fmt"

//https://medium.com/@Jonathan-Gibson/byte-sized-the-return-of-go-7f3dab9d8682

func main() {
	var arr [4]int
	arr[0] = 4
	arr[1] = 6
	arr[2] = 8
	arr[3] = 1

	array := []int{}
	array = append(array, 32)

	ar := make([]int, 0)
	ar = append(ar, 32)
	ar = append(ar, 32)
	ar = append(ar, 32)

	slice := ar[0:2]
	slice = append(slice, 32)
	var sl []int = arr[1:2]
	sl = append(sl, 32)
	fmt.Println(len(arr), " ", arr)

	obj := &a{name: "Manik", age: 24}
	err := aimpl(obj)
	if err != nil {

	}
	fmt.Println(obj)
}

func aimpl(param *a) error {
	param.name = "Sohel"
	return nil
}

type a struct {
	name string
	age  int
}
