package main

import (
	"context"
	"fmt"
	"sync"
)

// Main Source: https://betterprogramming.pub/how-to-broadcast-messages-in-go-using-channels-b68f42bdf32e
// Same : https://stackoverflow.com/questions/36417199/how-to-broadcast-message-using-channel

type BroadcastServer interface {
	Subscribe() <-chan int
	CancelSubscription(<-chan int)
}

type broadcastServer struct {
	source         <-chan int
	listeners      []chan int
	addListener    chan chan int
	removeListener chan (<-chan int)
}

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	// Generates a channel sending integers
	// From 0 to 9
	range10 := rangeChannel(ctx, 10)

	broadcaster := NewBroadcastServer(ctx, range10)
	listener1 := broadcaster.Subscribe()
	listener2 := broadcaster.Subscribe()
	listener3 := broadcaster.Subscribe()

	var wg sync.WaitGroup
	wg.Add(3)
	go func() {
		defer wg.Done()
		for i := range listener1 {
			fmt.Printf("Listener 1: %v/10 \n", i+1)
		}
	}()
	go func() {
		defer wg.Done()
		for i := range listener2 {
			fmt.Printf("Listener 2: %v/10 \n", i+1)
		}
	}()
	go func() {
		defer wg.Done()
		for i := range listener3 {
			fmt.Printf("Listener 3: %v/10 \n", i+1)
		}
	}()
	wg.Wait()
}

func (s *broadcastServer) Subscribe() <-chan int {
	newListener := make(chan int)
	s.addListener <- newListener
	return newListener
}

func (s *broadcastServer) CancelSubscription(channel <-chan int) {
	s.removeListener <- channel
}

// NewBroadcastServer function will be responsible for creating the broadcast server instance. It takes as input a context that handles proper
// cancellation and the source channel that will be broadcast.
func NewBroadcastServer(ctx context.Context, source <-chan int) BroadcastServer {
	service := &broadcastServer{
		source:         source,
		listeners:      make([]chan int, 0),
		addListener:    make(chan chan int),
		removeListener: make(chan (<-chan int)),
	}
	go service.serve(ctx)
	return service
}

func (s *broadcastServer) serve(ctx context.Context) {
	defer func() {
		for _, listener := range s.listeners {
			if listener != nil {
				close(listener)
			}
		}
	}()

	for {
		select {
		case <-ctx.Done():
			return
		case newListener := <-s.addListener:
			s.listeners = append(s.listeners, newListener)
		case listenerToRemove := <-s.removeListener:
			for i, ch := range s.listeners {
				if ch == listenerToRemove {
					s.listeners[i] = s.listeners[len(s.listeners)-1]
					s.listeners = s.listeners[:len(s.listeners)-1]
					close(ch)
					break
				}
			}
		case val, ok := <-s.source:
			if !ok {
				return
			}
			for _, listener := range s.listeners {
				if listener != nil {
					select {
					case listener <- val:
					case <-ctx.Done():
						return
					}

				}
			}
		}
	}
}

func rangeChannel(ctx context.Context, n int) <-chan int {
	valueStream := make(chan int)
	go func() {
		defer close(valueStream)
		for i := 0; i < n; i++ {
			select {
			case <-ctx.Done():
				return
			case valueStream <- i:
			}
		}
	}()
	return valueStream
}
