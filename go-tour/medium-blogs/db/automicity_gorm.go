package main

import (
	"errors"
	"gorm.io/gorm"
	"log"
	"main/medium-blogs/db/config"
	"strconv"
)

/*
Three ways to handle atomicity in GORM
1. Transaction: Gorm will do everything
2. Manually: Developer need to handle manually, like begin, rollback, commit
3. Savepoint-RollBackTo: Another way to handle manually
*/

type account struct {
	gorm.Model
	UserId  int `gorm:"unique"`
	Name    string
	Balance int
}
type withdraws struct {
	gorm.Model
	UserId int
	Total  int
}

func main() {
	err := config.InitGorm()
	if err != nil {
		log.Println("Failed to init db connection: ", err.Error())
	}
	db := config.GetGormConn()
	migrate(db)

	//acc := &account{UserId: 1, Name: "Manik", Balance: 500}
	//err = createAccount(db, acc)
	//if err != nil {
	//	log.Println("Failed to create account ", err)
	//}

	wd := &withdraws{UserId: 1, Total: 50}
	wd1 := &withdraws{UserId: 1, Total: 500}

	//Transaction handled manually
	err = withDraw(db, wd)
	if err != nil {
		log.Println(err)
	} else {
		log.Println("Successful withdraw money ", wd.Total)
	}
	//Transaction handled by Savepoint - RollBackTo
	err = withDraw1(db, wd1)
	if err != nil {
		log.Println(err)
	} else {
		log.Println("Successful withdraw money ", wd1.Total)
	}
	//Transaction handled by gorm:https://gorm.io/docs/transactions.html#Transaction:~:text=%2C%2018)-,Transaction,-To%20perform%20a
}
func createAccount(db *gorm.DB, acc *account) error {
	if err := db.Create(acc).Error; err != nil {
		return err
	}
	return nil
}

func withDraw(conn *gorm.DB, wd *withdraws) error {
	var acc account
	tx := conn.Begin()
	defer func() {
		_ = tx.Rollback()
	}()
	if err := tx.Create(&wd).Error; err != nil {
		return err
	}
	tx.Find(&acc)
	acc.Balance = acc.Balance - wd.Total
	if acc.Balance < 0 {
		return errors.New("failed to withdraw taka: " + strconv.Itoa(wd.Total))
	}
	if err := tx.Save(acc).Error; err != nil {
		return err
	}
	tx.Commit()
	return nil
}
func withDraw1(conn *gorm.DB, wd *withdraws) error {
	var acc account
	tx := conn.Begin()
	tx.SavePoint("beginning")
	defer func() {
		_ = tx.RollbackTo("beginning")
	}()
	if err := tx.Create(&wd).Error; err != nil {
		return err
	}
	tx.Find(&acc)
	acc.Balance = acc.Balance - wd.Total
	if acc.Balance < 0 {
		return errors.New("failed to withdraw taka: " + strconv.Itoa(wd.Total))
	}
	if err := tx.Save(acc).Error; err != nil {
		return err
	}
	if err := tx.Commit().Error; err != nil {
		return err
	}
	return nil
}

func migrate(db *gorm.DB) {
	db.AutoMigrate(&account{})
	db.AutoMigrate(&withdraws{})
}
