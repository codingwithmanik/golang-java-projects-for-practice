package main

import (
	"gorm.io/gorm"
	"log"
	"main/medium-blogs/db/config"
	"sync"
)

/*
Isolation ensures that all transactions run in an isolated environment. That enables running transactions concurrently because transactions
don’t interfere with each other.

For example, let’s say that our account balance is $200. Two transactions for a $100 withdrawal start at the same time. The transactions run in
isolation which guarantees that when they both complete, we’ll have a balance of $0 instead of $100.
*/
type counter struct {
	gorm.Model
	Count int
}

var wg sync.WaitGroup
var mutex sync.Mutex

func main() {
	err := config.InitGorm()
	if err != nil {
		log.Println("Failed to init gorm")
	}
	db := config.GetGormConn()
	//db.AutoMigrate(&counter{})

	increment(db, 50)
}
func increment(conn *gorm.DB, incBy int) {
	for i := 1; i <= incBy; i++ {
		wg.Add(1)
		go incByOne(conn)
	}
	wg.Wait()
}
func incByOne(db *gorm.DB) {
	defer wg.Done()
	count := counter{}
	mutex.Lock()
	if err := db.Where("id=?", 1).Find(&count).Error; err != nil {
		log.Println("Got error.types when finding count", err)
	}
	count.Count = count.Count + 1
	if err := db.Save(&count).Error; err != nil {
		log.Println("Got error.types when saving count", err)
	}
	mutex.Unlock()
}
