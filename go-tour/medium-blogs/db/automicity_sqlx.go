package main

import (
	"fmt"
	"log"
	"main/medium-blogs/db/config"
)

func main() {
	err := config.InitSQLX()
	if err != nil {
		log.Println("Failed to init db connection")
	}
	db := config.GetSqlxConn()
	fmt.Println("Got connection", db)
}
