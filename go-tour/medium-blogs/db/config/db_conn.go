package config

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	_ "github.com/newrelic/go-agent/_integrations/nrpq"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var gormDB *gorm.DB
var sqlxDB *sqlx.DB

func GetGormConn() *gorm.DB {
	return gormDB
}
func GetSqlxConn() *sqlx.DB {
	return sqlxDB
}

func InitGorm() error {
	dsn := "host=localhost user=postgres password=12345 dbname=test port=5432 sslmode=disable"
	DB, err := gorm.Open(postgres.Open(dsn), &gorm.Config{
		//Logger: logger.Default.LogMode(logger.Silent), //Silent: no logs will be printed by gorm
	})
	if err != nil {
		return err
	}
	gormDB = DB
	return nil
}

func InitSQLX() error {
	const connStr = "user=%s password=%s dbname=%s host=%s port=%d sslmode=disable"
	ds := fmt.Sprintf(connStr,
		"postgres",
		"12345",
		"test",
		"localhost",
		5432)

	db, err := sqlx.Open("nrpostgres", ds)
	if err != nil {
		return err
	}
	sqlxDB = db
	return nil
}
